require(tikzDevice)
source('../config.R')

tikz('regression-simple-influence.tikz.tex', width=gwidth[2]/2, height=gheight[2]);

here <- getwd()
setwd('listings')
source('ex-regression-simple-influence.R')
setwd(here)

dev.off()
