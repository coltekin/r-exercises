require(tikzDevice)
source('../config.R')

tikz('bgraph-scatter2.tikz.tex', width=gwidth[2]/2, height=gheight[2]);

source('listings/ex-bgraph-scatter2.R')

dev.off()
