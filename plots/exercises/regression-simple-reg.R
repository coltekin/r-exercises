require(tikzDevice)
source('../config.R')

tikz('regression-simple-reg.tikz.tex', width=gwidth[2]/2, height=gheight[2]);

here <- getwd()
setwd('listings')
source('ex-regression-simple-reg.R')
setwd(here)

dev.off()
