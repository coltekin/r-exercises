require(tikzDevice)
source('config.R')

tikz('lr-diag.tikz.tex', width=gwidth[2], height=gwidth[2]);

wcgrades <- read.csv('data/wcgrades.csv', head=T)
m <- lm(grade ~ wc, data=wcgrades)
par(mfrow=c(2,2))
plot(m)

dev.off()
