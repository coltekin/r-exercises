\section{\label{sec:data-manip}Advanced data manipulation}

This section does not contain any statistics related exercises, but
exercises about manipulating data in ways to get the right statistics.
First, we will review some data types \R{} handles (some new, some
familiar) and how to convert between them. Second, we will go a bit
further than Section~\ref{sec:data} in handling data in data frames.
You may not understand everything at one go here, but the knowledge
you gain here will come handy in a lot of cases. 

\subsection{\R{} data types}

So far we have worked with a number of \R{} data types. This subsection
will walk you through a few others, and introduce ways to better
control your data. As usual, we do not mean to be comprehensive, we
will certainly omit quite a lot of what \R{} offers.

The basic types in \R{} include, \emph{boolean}, \emph{numeric},
\emph{integer}, \emph{factor} and \emph{character}.  Booleans take
\lstinline{TRUE} or \lstinline{FALSE} (see also the discussion on
\lstinline{NA} below). Booleans are typically result of comparison
operations.  For \lstinline{TRUE} and \lstinline{FALSE}, you can use
shorthand versions \lstinline{T} and \lstinline{F}, respectively.
Note, however, that \lstinline{T} and \lstinline{F} are predefined
variables, and their values can be changed. \emph{numeric} does not
need much introduction. It is simply used where you need numbers.
\emph{integer} is a subset of \emph{numeric} for whole numbers.
\emph{factor} is used for categorical variables. Typical examples are
\emph{male} or \emph{female} for a gender variable, or \emph{noun},
\emph{verb}, \emph{adverb} etc.\ for a part-of-speech variable.
\emph{character} type is for strings of characters, such as
\lstinline{"this is a character"}.

The basic data types are suitable for stating a single data point for
a single variable. In statistics, we work with data where organized
collection of basic types are necessary. Previously we worked with
\emph{vector} and \emph{data frame}. Vectors are the simplest
way of representing a set of basic values. Remember that all values of
a vector has to be of the same type. Data frame's on the other hand
provide a data structure much like a spreadsheet. Typically, the
columns contain (vectors of) variables, and each row is a data point.

Besides \emph{vector} and \emph{data frame}, there are other objects
that collect multiple basic values. \emph{list}s are similar to
vectors. However, the elements of a list can have different types.
\emph{Matrix} data type is simply two-dimensional \emph{vector}.
\emph{Array} is N-dimensional generalization of vectors. Like elements
of \emph{vector}s, elements of a \emph{matrix} or an \emph{array} has
to be in the same type. \emph{table} is another type, basically an 
array of only integers, which is typically used for contingency
tables. 

In most cases, you do not need to worry about the data types. \R{} will
guess or return the correct type for most of the time.  However, if
output of a certain operation or analysis result in errors or warning,
or even looks suspicious, then it is likely that there is some
mismatch between what \R{} thinks your data type is, and what you
intended it to be. For example, if you are getting an unexpected
result for your linear regression, there is a chance that \R{} thinks
your numeric variable is a factor.

In an assignment operation, \R{} will guess the best possible data type
for the variable in question. However, you can also state explicitly
the type of the variable using the functions with the same name as the
types. For example, \lstinline{x = factor('1')} or 
\lstinline{i = integer(1)}.

The function \lstinline{class()} allows you to check the type \R{} uses
for a particular variable. The \lstinline{str()} function we used
before prints compact description of structure of an \R{} object, and
also prints out the class of the object and its children if exist. A
set of functions that return boolean values, such as
\lstinline{is.character()}, \lstinline{is.numeric()} and
\lstinline{is.data.frame()} can be used if you want to check if a
variable is in the respective class. When it makes sense, a set of
functions, such as \lstinline{as.character()},
\lstinline{as.numeric()} and \lstinline{as.factor()} can be used to
convert `coerce' between different types. 

Here are a few examples:

\lstinputlisting[numbers=left]{listings/data-types.txt}

Most of the examples above does not need much description. In
line~1 we create a data frame, and see that its class is
\lstinline{data.frame}. We can check each variable in data frame same
way as well, but \lstinline{str()} gives an easier way. We realize
that the variable \lstinline{c} is interpreted as a factor variable.
To stop \lstinline{data.frame()} to automatically convert strings to
factor, we use the parameter \lstinline{stringsAsFactors=F} in
line~19. Since this also prevents \lstinline{f} as a string we convert
\lstinline{f} to a factor variable explicitly in line~20. There are
other ways of achieving the same result.

\begin{Exercise}
Create the data frame \lstinline{x} in the example above, and convert the
variable \lstinline{n} to class \lstinline{integer}.
\end{Exercise}

\begin{Exercise}
Using, again, the data frame created above, check the result of the
command \lstinline{sum(x$b)}.%stopzone
Can you explain how the calculations may have been carried out for the
result you obtained?
\end{Exercise}

\subsection{Missing values}

Missing values are part of life in statistical analysis. We of course
want to have complete data sets, but it is often the case that
participants of a questionnaire do not answer some of the questions,
or we have to leave out some noisy measurements. \R{} allows a particular
value, \lstinline{NA}, to be specified in such cases. Almost all
operations involving a \lstinline{NA} will result in \lstinline{NA}.
Most functions in \R{}, such as \lstinline{sum} or \lstinline{mean},
in \R{}
include some way of controlling the behavior when data includes
\lstinline{NA}s.

\begin{Exercise}[label=data-na-read]
Read the data file \url{http://www.let.rug.nl/coltekin/R/data/simple-mlu.csv}
into a data frame, \lstinline{mlu}. The data file lists mean length
utterance (MLU) values (a measure of language proficiency/development) 
for four groups of children. Inspect your data. Do you see any unexpected 
values in the data?
\end{Exercise}
%\begin{Answer}[ref=data-na-read]
%\end{Answer}

\begin{Exercise}[label=data-na-read2]
In exercise~\ref{data-na-read}, you should have spotted (at least) the 
following problems:
\begin{itemize}
\item \R{} takes \lstinline{age.gr} variable as numeric, even though it
should be categorical.
\item One of the MLU values are -1. We will assume that this is the
convention used during data entry to specify missing values.
\end{itemize}

To fix these problems, re-read the data using \lstinline{read.csv()}
to make sure \lstinline{age.gr} is a factor, and \lstinline{-1} values
are automatically converted to \lstinline{NA}. \textbf{TIP:} options
\lstinline{colClasses} and \lstinline{na.strings} will be useful.
\end{Exercise}
%\begin{Answer}[ref=data-na-read2]
%\end{Answer}


To check whether a particular value is \lstinline{NA} or not, you can
use the function \lstinline{is.na()}. Besides \lstinline{NA}, there is
one more value that qualifies as \lstinline{NA}. The numeric results
that result in an undefined value (e.g. 0 divided by 0, or
logarithm of a negative number) results in \lstinline{NaN} (not a
number), which also qualifies as \lstinline{NA} (but the reverse is
not true).


\subsection{Factor variables}

The categorical variables, such as \emph{gender}, \emph{language},
\emph{education}, \emph{part of speech} play an important role in many
statistical analysis tasks. You probably already realized that various
functions, such as \lstinline{str()}, lists a set of levels when
summarizing factor variables. The factor variables are associated with
a set of values, \emph{levels}, that are initialized when they are
created. So far, we trusted \R{} to assign correct levels, but there are
cases when you may need to work with the levels of a factor variable
manually. We will work through a few common cases here. Here is
a few examples that should get you started.

\lstinputlisting[numbers=left]{listings/data-factor.txt}
New part of the exercise starts on line~12, where we list the
levels of the variable, and on line~14 where we re-set the level
labels, such data the all data points with label \lstinline{a} and
\lstinline{c} are swapped. That may seem somewhat artificial, but we
will soon see when/why you may need such manipulations.

\begin{Exercise}[label=data-factor-boxplot]
Using the \lstinline{mlu} data you prepared in Exercise~\ref{data-na-read2}, 
draw side-by-side box plots for each age group. Remember that you can
use the formula notation (\lstinline{mlu ~ age.gr}) with \lstinline{boxplot}.
\end{Exercise}
%\begin{Answer}[ref=data-factor-boxplot]
%\end{Answer}

\begin{Exercise}[label=data-factor-swaplevel]
Exercise~\ref{data-factor-boxplot} points out an interesting
discrepancy about our \lstinline{mlu} data. It seems the age group `2'
has smaller MLU values compared to `1'. Suppose that this is due to 
an error during data entry. Change the levels such that the levels `1'
and `2' are swapped. Plot another box plot to see if everything worked
as expected.
\end{Exercise}
%\begin{Answer}[ref=data-factor-swaplevel]
%\end{Answer}


\begin{Exercise}[label=data-factor-orderlevel]
Exercise~\ref{data-factor-swaplevel} fixes the data entry problem.
However, the box plot you produced in this example still looks odd,
since the age group `2' is shown first. You can use the function
\lstinline{relevel} (or more generally \lstinline{reorder()}) to
re-order the levels of a factor variable. Order the levels of
\lstinline{mlu$age.gr}%stopzone
such that the age groups are displayed in correct order.
\end{Exercise}
%\begin{Answer}[ref=data-factor-boxplot]
%\end{Answer}

\begin{Exercise}[label=data-factor-dellevel]
The age group `4' in the data frame we have been working with seems to
have too few variables to be of any interest. You can use the command 
\lstinline{mlu <- mlu[mlu$age.gr != '4',]}%stopzone
removes the data points where age group is 4 (we will shortly
introduce what the command means). Remove these data points, and plot
another side-by-side box plot showing MLU values for each group. 

You should see that \lstinline{boxplot()} still includes the level `4'.
Fix this problem by removing the value \lstinline{'4'} from the levels
of the variable.
\end{Exercise}
%\begin{Answer}[ref=data-factor-dellevel]
%\end{Answer}

\subsection{Accessing elements of a vector or a data frame}

We already know how to access individual elements of a vector or data
frame from Section~\ref{sec:vector} and \ref{sec:data}. If you do not,
you should start reviewing these sections. There will be an overlap
between this section and the previous discussion. However, here we
will go a bit more into detail. The examples below are all for data
frames, but the way you specify indexes does not change for vectors
either. Here is a listing demonstrating displaying selected parts of
a data frame (description follows the listing).

\lstinputlisting[numbers=left]{listings/dataframe-select.txt}

As line~13 demonstrates, you can select a particular value at row
\lstinline+x+ and column \lstinline+y+ by \lstinline+bc[x,y]+. You can
select a row without specifying a column, in which case you get
specified row with all the columns. Similarly, you can select the
columns, either by their index as in line~18, or by their name with
the notation in line~20. There is a fine difference between selecting
rows and columns: \lstinline+bc[1,]+ above returns a data frame, but
\lstinline+bc[,2]+ or \lstinline{bc$freq}%stopzone
returns a vector (you can
actually get a data frame including the column if you do not use any
commas, for example \lstinline+bc[2]+). 


The command on line~24 demonstrates the way to select ranges for both
rows and columns. In this expression the conditional expression before
the comma select the rows where \lstinline+freq+ is higher than its
mean value, and the range expression after the comma selects the first
two columns. Note that, as the last command on line~29 shows, the
selection does not have to be a continuous range of rows or columns.
Let's have a look at the column expression first, which is easier to
grasp. We are simply asking index numbers \lstinline+1+ and
\lstinline+3+, corresponding to \lstinline+word+ and \lstinline+pos+
columns respectively. The row selection specification,
\lstinline+-c(1:5, 8, 10)+, is a bit complicated. The vector formed by
\lstinline+c()+ contains the range 1--5, 8 and 10. So it is the same
as \lstinline+c(1,2,3,4,5,8,10)+.  The minus sign `\lstinline+-+' at
the beginning tells \R{} that we want the rows whose indexes are not in
this vector. As a result, we actually get the rows \lstinline+6+,
\lstinline+7+ and \lstinline+9+.  Note that using minus notation, we
could use the column selection expression \lstinline+-c(2)+ or simply
\lstinline+-2+ for the same effect.

** In \R{}, there are actually two ways of specifying index vectors. First,
if you specify a numeric vector as index, such as
\lstinline+c(1,3)+ or \lstinline+1:2+, \R{} will select the values
corresponding to these indexes. The other way, we used above
implicitly is to specify a \emph{boolean} vector. If you specify a
boolean vector exactly the same length as the rows or columns, the
rows or columns that correspond to \lstinline{TRUE} in the index vector 
will be selected. To see how it works, we type the following (which is
the statement we used earlier to select the rows with frequency over
the mean value)
\lstinputlisting{listings/dataframe-boolindex.txt}
Note that this is simply a vector of length 10 (same as number of
rows), and the first three members are \lstinline+TRUE+. As a result
\R{}
selects first three rows, and ignores the rest when you specify this
vector as index vector for rows. If you grasp this basic idea, working
with data frames in \R{} becomes easier, and you can do many things that
takes hours in an interactive program, within a matter of minutes or 
seconds (See exercise~\ref{ex:dataf-conversion}).

\subsection{Some common operations on data frames}

Given a data frame, we can use most of the common functions we use on
scalar values and vectors. However, since a data frame can contain
different data types, not every function will be applicable. For
example if we use the \lstinline+sum()+ on our example data set, 
\begin{lstlisting}
> sum(bc)
Error in FUN(X[[1L]], ...) :
  only defined on a data frame with all numeric variables
\end{lstlisting}
\R{} will rightfully complain that it cannot sum non-numeric variables.
But other generic functions will work. Here are two examples:
\lstinputlisting{listings/dataframe-head.txt}
Note that for non-numeric variables, \R{} gives you a different 
sort of summary.

\begin{Exercise}[label=ex:dframe-simple-select]
Calculate the sum of the \lstinline{freq} variable.
\begin{Answer}

\lstinputlisting{listings/ex-df-simple-select.txt}
\end{Answer}
\end{Exercise}

Another operation we often do on a set of data is to sort it according
to a certain variable (or a set of variables). You will probably find
the way to do that in \R{} rather confusing at first, but once you
understand it, you will also see that it is actually simple and
flexible. If we wanted to get a sorted data frame by frequency, but
in ascending order, the way to do it is as follows:

\lstinputlisting{listings/dataframe-order.txt}
First, note that the notation looks very similar to row selection we 
have studied before. If you want to see what is happening, you can
type \lstinline{order(bc$freq, decreasing=F)}%stopzone
in \R{} command prompt
and examine the result. \lstinline+order()+ does not return a sorted
value, it actually returns a vector of index values, which in turn
selects the rows of interest in the order specified by the index
values, which is also evident in the index values printed in the first
column of the above listing.

Now, let's try to sort the data frame according to \lstinline+word+
variable.
\lstinputlisting{listings/dataframe-orderstr.txt}
The result you get may be somewhat different. However, unless you are
really lucky, you should not get a data frame that is sorted according 
to lexicographic order of the words. Let's try to see what is happening, 
we use another useful function:

\lstinputlisting{listings/dataframe-str.txt}

As the above listing shows, the reason is that \R{} does not interpret
this variable as a character string. Rather, it treats both
\lstinline+word+ and \lstinline+pos+ variables as \emph{categorical}
variables or, as \R{} calls them, \emph{factor}s. As a result sort order
is not what we expect from a character string. For the solution, you
should convert the factor values character strings using the function
\lstinline{as.character()}.

\begin{Exercise}[label=ex:dframe-sort-str]
Display the data frame \lstinline{bc} sorted according lexicographic
order of the \lstinline{word} variable.
\begin{Answer}

\lstinputlisting{listings/ex-dframe-sort-str.txt}
\end{Answer}
\end{Exercise}

This distinction between \emph{factor} and \emph{character} string
variables in \R{} can particularly be important while doing linguistic
analysis. For example, you can calculate the length of a character
string or check if the first letter is `\lstinline+a+' or not, but
these do not make sense for factor variables.

When we have a factor variable, we often want to do certain operations
for the other variables for each \emph{level}, or value, of the factor
variable. \R{} provides a helpful function to do that. The partial
listing below shows application of \lstinline+summary()+ function to
all variables of \lstinline+bc+ data frame for each value of variable
\lstinline+pos+.

\lstinputlisting[linerange=1-13]{listings/dataframe-by.txt}
\noindent\raisebox{3ex}{\ldots}

Before stopping the long introduction to using data frames in
different ways, we will mention one last command \lstinline+attach()+
that may save you from considerable amount of typing. When you work on
a data set for a long time, even a short data frame name like
\lstinline+bc+ may make you feel threatened by repetitive strain
injury. The function \lstinline+attach()+ allows you to access the
variable names in your data frame directly without typing the name of
the data frame.

\lstinputlisting[linerange=1-10]{listings/dataframe-attach.txt}
\noindent\raisebox{3ex}{\ldots}

As shown, after \lstinline{attach}ing, \lstinline{bc} we can use the
variable names \lstinline{freq} and \lstinline{pos} directly, without
prefixing with \lstinline{bc$}.%stopzone 
If you do not want to access the
variable names of a data frame directly, you can use the function
\lstinline{detach()} to remove the effect of \lstinline{attach()}.

\subsection{Adding and removing rows and columns from a data frame}

We have already mentioned how to add columns to a data frame in
Section~\ref{sec:data}. Here we will try to get a more complete
picture of how to add and remove columns or rows from a data frame.
The output is not included here to save space, but you should type
these commands on \R{} command line and see if the result is what you
expect.

\begin{itemize}
\item Removing rows is nothing different than selecting certain rows.
Note that you need to assign the result back to the same variable to
actually remove it (not done in the listing below).
\begin{lstlisting}
bc[!(as.numeric(rownames(bc)) %in% c(2, 5)),]
bc[as.numeric(rownames(bc)) <= 5,]
bc[bc$pos != 'P',]%stopzone
\end{lstlisting}

Note that exclamation mark `\lstinline{!}' on the first line is
logical negation. The operator \lstinline{%in%} returns true for rows
2 and 5, and \lstinline{!} negates it. Other operators that may need
introduction are \lstinline{<=} which means \emph{less than or equal
to}, and \lstinline{!=} which means \emph{not equal to}. The equality
operator is \lstinline{==} (double equal sign). Remember that single
equal sign is the assignment operator.

As the name suggests, the function \lstinline{rownames()} returns
names of the rows. So far the row names we've seen have been numeric
index values.  However, they are still `names', as a result you need
to convert them to a number before using as index value.
\item Removing columns is similar, 
\begin{lstlisting}
bc[,!(names(bc) %in% c('pos')),]
\end{lstlisting}
\item Adding rows also comes with its complications. The most general
method is,
\begin{lstlisting}
rbind(bc, data.frame(word='for', freq=8987, pos='P'))
\end{lstlisting}
Note that what this does is first to create a new data frame with the
same variables, and bind these two data frames together. This may be
slow if you are adding lines to a data frame repeatedly, 
you may want to use,
\begin{lstlisting}
bc[11,] = list('for', 8987, 'P')
\end{lstlisting}
however, this will be problematic for factor variables, as \R{} will not
automatically add the factor variables that do not exist in the
original data frame.
\item Adding columns is simpler once you know the syntax. For example,
the following will create a new column, which contains rank of the
item when sorted  by frequency. You can of course add arbitrary
values, with any variable name. However, the size of the vector should
be equal to the number of rows in the data frame.
\begin{lstlisting}
bc$rank = order(bc$freq, decreasing=T)
\end{lstlisting}

In some cases, you want to add the column without any value, or a
default value, and fill it in as data becomes available. In that case
you may do something like,
\begin{lstlisting}
bc$rank = rep(NA, length(bc$freq))
\end{lstlisting}

The special value `\lstinline{NA}' stands for \emph{not available},
and used for missing data. The function \lstinline+rep()+ repeats a
given value number of times given in its second argument, and returns
as a vector. As a result, this command creates a new vector filled
with \lstinline{NA}'s as many times as the length of
\lstinline{bc$freq} and add it to \lstinline{bc} as a new column
named \lstinline{rank$}.
\end{itemize}


\subsection*{Exercises}

\begin{Exercise}[label=ex:dataframe-sort]
Sort the data frame \lstinline{bc.caps} by total frequency, displaying the
most-frequent word on top.
\begin{Answer}

\lstinputlisting{listings/ex-dataframe-order.txt}
The answer is rather straightforward, but note the use of
\lstinline{attach()} on the first line. This, of course, is 
not necessary, but it allows us to use the variable names 
in the data frame directly.
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:dataframe-newrow]
Add a new row with the mean of each column.
\begin{Answer}

\lstinputlisting{listings/ex-dataframe-newrow.txt}
This demonstrates a solution with the methods we learned so far. 
If you are looking for a neater solution that would also prevent
excess typing, You an have a look at functions \lstinline{colMeans()} or
\lstinline{sapply()}.
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:dataframe-delrow]
Delete the row you added in Exercise~\ref{ex:dataframe-newrow}.
\begin{Answer}

Any of the following should work:
\begin{lstlisting}
> bc.caps = bc.caps[1:6]
> bc.caps = bc.caps[bc.caps$word != 'MEAN',]%stopzone
\end{lstlisting}
\end{Answer}
\end{Exercise}


\begin{Exercise}[label=ex:dataf-conversion]
The data file we used in Exercise~\ref{ex:dataframe-import} above
suffers from one of the common data entry convention differences.
For use in \R{}, we normally want
data on the same row to be related, columns to correspond variables of
interest, and each variable to be presented on a single column. In
other words, our data should have looked like the following

\begin{center}
\begin{tabular}{lrr}\toprule
reaction.time & condition \\
\toprule
641 & control\\
734 & control \\
802 & patient \\
\ldots & \ldots \\
\bottomrule
\end{tabular}
\end{center}

Convert the data in \lstinline+rt+ to follow this convention, name the
new data frame \lstinline+rt2+. Use \R{} table manipulation functions:
do not recode the data manually (for data of this size it is
tempting to do it manually, but consider you needed to do a similar
conversion for Brown corpus which has about 55000 word types.).

\textbf{TIP:} \lstinline+rbind()+ and \lstinline+rep()+ are your friends.
\begin{Answer}

The single command does it all. All components used should be
familiar.
\lstinputlisting{listings/ex-dataframe-conversion.txt}
\end{Answer}
\end{Exercise}


\vspace{2mm} \noindent For the rest of the exercises in this section,
use the full Brown corpus data as previously read into data frame
\lstinline{bc.all}. If it is not available in your \R{} environment
(\lstinline{ls()} answers this question), you should load it, and it
may be a good idea to \lstinline{attach} the data frame. Here is how
to do both:

\begin{lstlisting}
bc.all = read.delim('data/brown.csv', sep='\t', quote='"', header=T,
                 colClasses=c('numeric', 'character', 'factor', 'character'))
attach(bc.all)
\end{lstlisting}

%\begin{Exercise}
%Display a histogram of the variable \lstinline+freq+.
%\end{Exercise}
%%\begin{Answer}
%%%~~~
%%\end{Answer}

%\begin{Exercise}
%Find the frequencies of each part-of-speech tag.
%\end{Exercise}
%%\begin{Answer}
%%~~~
%%%\end{Answer}

\begin{Exercise}
Find the number of rows where the lemma and the word are the same.
\begin{Answer}
Of many ways of doing it, the following listing demonstrates three:
\lstinputlisting{listings/ex-dataframe-eqcount.txt}
The first command literally does what we asked for: displays the
length of the vector which lists the words the words which are the
same with their lemma.

The second one is a trick. First, remember that 
\lstinline{word == lemma} produces a vector of boolean 
values, \lstinline{TRUE} and \lstinline{FALSE}. When used 
in an arithmetic operation, \lstinline{TRUE} is converted 
to 1, and \lstinline{FALSE} is converted to 0. So summing 
all up will count the number of \lstinline{TRUE} instances.

Last command, \lstinline{table()} is normally used for creating
\emph{cross tabulations} (or \emph{contingency tables}) to which we
will return later. When given a single variable, it plots a
one-dimensional table, i.e., it displays count of each value.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Display first 10 rows of all closed-class words. We define the 
closed-class words as 
having any \lstinline+pos+ except one of
\lstinline+ADJ+, \lstinline+ADV+, \lstinline+N+, \lstinline+NP+,
\lstinline+NUM+, \lstinline+V+, \lstinline+VD+, \lstinline+VG+, 
\lstinline+VN+.
\begin{Answer}

\lstinputlisting{listings/ex-dataframe-rowsel1.txt}
All parts of this should be familiar by now. The temporary variable is
used just for clarity, you could type the value instead in the main
expression.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Repeat the previous exercise, but use the fact that all and the
only part of speech tags start with one of 'A', 'N' or 'V' are open-class
words.
\textbf{TIP:} you need the function \lstinline{substr()}.
\begin{Answer}

\lstinputlisting{listings/ex-dataframe-rowsel2.txt}
The trick in this exercise is to remember that we have a factor field,
and to be able to use \lstinline{substr()} to extract the first
character, we need to convert it to a characters string first.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Add a new column to \lstinline{bc.all} which contains the rank of the
word. Note that \lstinline+order()+ will not behave as you want when
there are ties (and there will be). You may want to check 
\lstinline+rank()+ for that purpose.
\begin{Answer}

\lstinputlisting{listings/ex-dataframe-rank.txt}
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:datafrmae-zipf]
\begin{enumerate}
\item Plot \lstinline{freq} (on Y-axis) against \lstinline{rank} (X-axis), 
use lines instead of points and plot for ranks upto 1000. Do you 
see the well-known Zipfian-like distribution?
\item Plot \lstinline{log(freq)} against \lstinline{log(rank)}. 
\item Plot a straight line matching approximately the log-log plot on
the same graph, using a different color. You do not need to calculate
the bets fit (yet), you should decide for the intercept and the slope
visually.
\end{enumerate}
\end{Exercise}
%\begin{Answer}[ref=ex:datafrmae-zipf]
%
%\lstinputlisting{listings/ex-dataframe-zipf.txt}
%
%\SetTikzExport{png}
%\resizebox{\textwidth}{!}{
%\tikzinput{plots/exercises/dataframe-zipf}
%}
%\ReSetTikzExport
%\end{Answer}

%\begin{Exercise}
%Replace the frequency column with relative frequency. This time use
%the actual count of the words.
%\end{Exercise}
%\begin{Answer}
%~~~
%\end{Answer}

