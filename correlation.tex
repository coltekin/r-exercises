\section{\label{sec:corr}Correlation}

Often, we want to know whether we can predict the value of a variable
from another one. Correlation is a standardized measure indicating to
which degree two variables vary together.

In this section, we will work with two datasets, \lstinline{bc.caps} and
\lstinline{bc.all}, we have used in Section~\ref{sec:data}. If you do
not have these data frames in your current \R{} environment, the
following commands should load them:

\begin{lstlisting}
> bc.caps = read.csv('http://www.let.rug.nl/coltekin/R/data/bc-caps2.data')
> bc.all = read.table('http://www.let.rug.nl/coltekin/R/data/brown.csv', 
                sep='\t', quote='"', header=T,
                colClasses=c('numeric', 'character', 'factor', 'character')))
\end{lstlisting}

In \R{}, we can find the correlation between any two variables using the
function \lstinline{cor()}. Here is a simple example, where we are
asking the question whether uppercase frequency is correlated with the
lowercase frequency. You should already have an idea if these two
variables are correlated from the scatter plots in exercises 
\ref{ex:dataframe-scatterp} and \ref{ex:dataframe-textp}. Nevertheless
here is the numeric calculation:

\lstinputlisting{listings/cor-simple.txt}

\lstinline{cor()} returns a single correlation coefficient. Remember
that correlation coefficient ranges between -1 and 1. A value of 0
means no correlation at all, both extremes mean perfect correlation.
But a negative sign indicates that higher values of one of the
variables is correlated with lower values of the other. 

In a large number of cases, what we'd like to do is to inspect
correlation of multiple variables. Of course, we can run
\lstinline{cor()} multiple times, but if our variables are nicely
present in a data frame we can also do with a single run:

\lstinputlisting{listings/cor-matrix.txt}

By selecting columns 2--4, we provided a data frame with numeric
values only. If given a data frame instead of two simple variables,
\lstinline{cor()} calculates the correlation coefficients for each
pair of columns in the data frame, and present the result in a symmetric matrix. This
result indicates that all three variables are correlated. However, it
seems the correlation of the total frequency and lower-case frequency is stronger than the
correlation of the uppercase frequency with others.

If you are reading carefully so far, you would realize that
\lstinline{cor()} only returns the correlation coefficients, and it
does not return any significance or reliability indication. For this
purpose you need to use \lstinline{cor.test()}:

\lstinputlisting{listings/cor-test-simple.txt}

Now we get a p-value. Furthermore, output also tells us which
correlation coefficient is being calculated. 


By default, \lstinline{cor()} and \lstinline{cor.test()} calculate
\emph{Pearson's product-moment correlation coefficient}, commonly
called \emph{Pearson's r}. Pearson's r require the variables to be
distributed (approximately) normally. When normality assumption does
not hold, we can fall back to non-parametric alternatives. Both
\lstinline{cor()} and \lstinline{cor.test()} implement non-parametric
alternatives \emph{Spearman's $\rho$ (rho)} and \emph{Kendall's $\tau$
(tau)} as well. You can choose the method using the
\lstinline{method} option. 

\subsection*{Exercises}

\begin{Exercise}
The default correlation coefficient, Pearson's
product-moment correlation requires the samples to be distributed
approximately normally. Check whether the three variables in 
\lstinline{bc.caps} data set are distributed normally or not.
\begin{Answer}

\lstinputlisting{listings/ex-cor-normality.txt}
\tikzinput{plots/exercises/cor-normality}
All points in all three Q-Q plot seems to lined up nicely, except for
one in each graph. Although it may be tempting to ignore it, 
\lstinline{shapiro.test()} seems to indicate significant non-normality
for two of the distributions, and in one of them we fail to reject the
null hypothesis (that the data is normally distributed) only
marginally. Deciding normality from only 6 points is indeed difficult.
But since we also know that these values are word frequencies, our
knowledge should also support that the samples should not be normally
distributed. As a result earlier correlation and significance values
we obtained are likely to be wrong.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Try all three methods allowed in \lstinline{corr.test()} 
to calculate the correlation between \lstinline{freq.upper} and
\lstinline{freq.lower}.
\begin{Answer}

\lstinputlisting{listings/ex-cor-nonparametric.txt}
The first test, with \lstinline{method="pearson"}, is the
repetition of the previous example since default method is Pearson's
product-moment correlation. Note that the parametric \emph{pearson}
test gives a rather strong significant (at $\alpha$=0.5) correlation 
(0.85). However, when we apply the non-parametric tests, the
correlation is not significant, furthermore, the coefficients found by
both methods are negative!

The morale of the story is if you ever need to report a correlation
coefficient, make sure that you know what you are reporting, and
reporting it right.
\end{Answer}
\end{Exercise}

\begin{Exercise}
\begin{itemize}
\item Create a data frame named \lstinline{rnd} with two
variables \lstinline{x} and \lstinline{y} which hold two samples 
of 100 random data points that are distributed according standard normal
distribution.\\
\textbf{Reminder:} You can generate the samples with \lstinline{rnorm()}.
\item Find the correlation between \lstinline{x} and \lstinline{y}, is
your result significant?
\item Add two more columns to your data frame,
    \begin{description}
    \item[x.plus.x] which contains $2 \times x$.
    \item[x.squared] which contains $x^2$.
%    \item[x.plus.y] which contains $x+y$. 
%    \item[x.times.y] which contains $x \times y$. 
    \end{description}
\item Calculate correlation matrix for all variables in data frame
\lstinline{rnd}. Are the values you see in the matrix what you expect?
Particularly, how do you explain the differences between the
correlations of $x$--$2x$ and $x$--$x^2$?
\item Do non-parametric correlation coefficients give a better result
for the correlation between \lstinline{x}--\lstinline{x.squared}?
\end{itemize}
\begin{Answer}

\lstinputlisting{listings/ex-cor-random.txt}
The usual note applies here: your results may be somewhat different due to
randomness. What the long listing above demonstrates can be summarized 
as follows: 
\begin{itemize}
\item Independent random samples do not correlate. You can find chance
correlations if your samples are small though.
\item All correlation coefficients, including the non-parametric ones,
assume existence of a \emph{linear} correlation: sums are fine ($x$
and $2x$ correlate perfectly. However, even though there is a perfect
(non-linear) correlation between $x$ and $x^2$ the correlation tests
will not reveal that (The fact that Kendall's $\tau$ indicates a weak 
correlation in the example above does not change the fact that it 
all miss the existence of a perfect non-linear correlation).

\item If you suspect a correlation, you should always plot a scatter plot
and try to inspect the possibility of non-linear correlations visually.
You can, then, transform your data, and also check it formally.
\end{itemize}

\end{Answer}
\end{Exercise}

\begin{Exercise}
\end{Exercise}

\begin{Exercise}[label=ex:cor-bc]
Using the \lstinline{bc.all} data set, 
\begin{itemize}
\item Check correlation between \lstinline{freq} and 
\lstinline{rank} Pearson's and Spearman's methods. 
\item Check correlation between logarithm of \lstinline{freq} and 
logarithm of \lstinline{rank} using the methods you used above.
Can you explain the differences between the methods before and after
the log transformation?
\item[\textbf{**}] Check whether there is a significant correlation between 
length of the words and their frequency. Make sure you are using the
correct correlation coefficient and make necessary transformations if
necessary.
\end{itemize}
\begin{Answer}

\lstinputlisting{listings/ex-cor-bc.txt}

For the first two commands above, there is nothing different but a
bigger data set. However, while the Pearson's correlation coefficient
shows a low negative correlation, after log transformation it gives an
almost perfect negative correlation. Simply, the correlation is
normally non-linear, and log-transformation (as we have seen in the
previous section) makes the relationship linear.
Spearman's $\rho$ shows a perfect negative correlation in both cases.
Note that $\rho$ is a rank-based measure, and by having ranks of
frequencies, we basically ensure that a rank-based measure would reveal
the correlation, and since log transformation preserves the order, 
it does not affect the result.

For the last question, we choose to log-transform both length of the
words and the frequency. Unfortunately, \lstinline{shapiro.test()}
does not work with large number of data points. However, we can plot
Q-Q plots of the raw sample, and log transformation of it to check the
results. Particularly for word frequencies, the log-normal assumption
is a common (but not so accurate) in the literature. The word-length
distribution in this sample looks more like normal after
log-transforming it. You should try to inspect the samples for
normality using Q-Q plots, and possibly checking the histograms. The
listing above also shows the Sparman's method, which also indicate a
similar correlation.

In answer of this exercises we did not use Kendal's $\tau$ mostly
because it requires more computing time. However, it is an (arguably
better) alternative to $\rho$ when normality assumption does not hold.
However, one more reminder, linearity assumption is a requirement for
all correlation measures. Before making any generalizations,  you
should always check whether the relation between the variables is
linear or not.
\end{Answer}
\end{Exercise}
