#!/usr/bin/python2.7

import nltk
from nltk.stem.wordnet import WordNetLemmatizer

lem = WordNetLemmatizer()


nltk2wordnet = { "ADJ" : "a", "ADV" : 'r', "CNJ" : 'n', "DET" : '',
                 "EX" : '', "FW" : '', "MOD" : 'v', "N" : 'n',
                 "NP" : 'n', "NUM" : '', "PRO" : '', "P" : '',
                 "TO" : '', "UH" : '', "V" : 'v', "VD" : 'v', 
                 "VG" : 'v', "VN" : 'v', "WH" : '' } 

#nltk2wordnet = { "ADJ" : "a", 
#                 "ADV" : 'r', 
#                 "MOD" : 'v', 
#                 "N" : 'n',
#                 "NP" : 'n', 
#                 "V" : 'v', 
#                 "VD" : 'v', 
#                 "VG" : 'v', 
#                 "VN" : 'v' } 

tw = nltk.corpus.brown.tagged_words(simplify_tags=True)

for (w, pos) in tw:
    if pos not in nltk2wordnet:
        continue
    if nltk2wordnet[pos]:
        l = lem.lemmatize(w.lower(), nltk2wordnet[pos])
    else:
        l = lem.lemmatize(w.lower())
    if l.endswith("'"):
        l = l[:-1]
    elif l.endswith("'s"):
        l = l[:-2]
#    print "%s\t%s\t%s\t%s" % (w, w.lower(), pos, l)
    print w
