\section{\label{sec:start}Starting \R{} and finding your way around}

Depending on the environment or operating system you are using,
starting \R{} may be a bit different. Typically you will click on the
relevant icon or menu item, or on UNIX-like systems you can run the
command \lstinline{R} on the shell prompt. 

When you start \R{}, it will greet you with something similar to the
following:

\lstinputlisting[frame=single,language={}]{listings/startup.listing}

First thing you need to get used to (if you are not already) is that
\R{}
is controlled through a command line interface. The last line in the listing
above, \lstinline+>+, is the \emph{command prompt}. \R{} presents this
prompt when it is ready to accept commands.

The command-line interface may feel awkward or old-fashioned at first,
but once you get used to, you will see that it is not as scary as it
may seem at first sight, and it has its advantages in many cases.
The greeting message you see at the startup already gives you a few
tips. Now type 
\begin{lstlisting}
> help()
\end{lstlisting} 
including the parentheses but not the command prompt.  In this
tutorial we will follow this convention: the commands you should type
will be displayed after the command prompt, `\lstinline+> +'.

If you type the above command and press enter, \R{} will present the
online documentation about how to get help. Depending on the \R{}
configuration on your system, you may get the help window either on
the same window, or \R{} may present you another
window. If you get help on the same window,  you can scroll up and
down using arrow keys or page up/down keys on your keyboard. Pressing
`\lstinline{q}' will quit help and give the command prompt back.
As you were instructed at the greeting message, you could
alternatively type 
\begin{lstlisting}
> help.start()
\end{lstlisting}
and get the documentation in an external browser. To get help on a
particular command, for example \lstinline+pnorm+, you can type 
\begin{lstlisting}
> help(pnorm)
\end{lstlisting}
but in case you do not remember the exact command, you can search a
keyword in the documentation using \lstinline+help.search()+. For
example, if you were wondering what was the command that did
\href{http://en.wikipedia.org/wiki/Student\%27s_t-test}{Student's T test}, 
you can try
\begin{lstlisting}
> help.search('student')
\end{lstlisting}
\R{} will list the help topics that match, and you can again use
\lstinline+help+ to read the documentation. Two shortcuts you may
appreciate if you use the help facility frequently are \lstinline+?+
and \lstinline+??+ which correspond to \lstinline+help()+ and
\lstinline+help.search()+ respectively. When using \lstinline+?+ and
\lstinline+??+, you should just type the keyword(s) without
parentheses. If the keyword contains white spaces, you need to use
double or single quotes around it.

A tip that you may be happy to hear is that \R{}
remembers your previous commands. You can return to the previous
commands using the \emph{up} arrow key on your keyboard, navigate
between them with \emph{up} and \emph{down} arrow keys, and  you can
modify and re-run them if you wish.

There are many small tips and tricks you will collect while working
with \R{}, one last tip to mention here is that \R{} command line allows `tab
completion'. That is, if you type a unique initial segment of a
command (or variable or file name in the right context), and press the `tab'
key on your keyboard, \R{} will try to complete the rest for you. If there are
more commands that match the initial string you typed, pressing `tab'
twice will list all matching commands.

\subsection*{Exercises}

%\begin{ExerciseL
%\begin{questions}
%\Exercise 
\begin{Exercise}
You want to do a `Multivariate ANOVA', but you do not know
the exact command that does it. Use \lstinline+help.search()+ (or
\lstinline+??+) to find the name of the command.

%\hyperlink{ans:help}{answer}
\begin{Answer}
\begin{lstlisting}
> ??'multivariate anova'
stats::manova           Multivariate Analysis of Variance
stats::summary.manova   Summary Method for Multivariate Analysis of
                        Variance
\end{lstlisting}
So, \lstinline+manova()+ should be the function we are looking for.
Now it's time to type \lstinline{?manova} and learn how to use it.
Note that some additional information on the output is skipped in this
listing.
\end{Answer}
\end{Exercise}

\begin{Exercise}
The \R{} function \lstinline+shapiro.test()+ implements well-known
Shapiro-Wilk normality test. 
    \begin{enumerate}
    \item How many of the initial characters you need
type before \R{} can complete the function name using tab completion?

    \item How many \R{} functions start with \lstinline+sh+?
    \end{enumerate}
\begin{Answer}

The answer may change depending on the version of \R{} and the
packages loaded. On my system if I press tab after the third letter
(the initial segment \lstinline{sha}) the full function name is
displayed. And, after \lstinline{sh} pressing tab twice gives me a
list of 9 commands.
\end{Answer}
\end{Exercise}
%\end{questions}
%\end{ExerciseList}

%{\raggedleft \hyperlink{ans:start}{Answers}\\}

%TODO: 
% - (?) secondary command prompt
% - (?) help on the web
