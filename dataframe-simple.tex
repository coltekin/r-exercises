\section{\label{sec:data}Data frames}

Vectors in \R{} are good for storing a sample of a single variable.
However, we typically work with multiple variables.  We can of course
store different variables in different vectors. However, we would like
to keep our data in nice orderly tables. The structure \R{} offers for
storing this type of data is called \emph{data frame}. You can think
of a data frame as a spreadsheet, and actually you will most likely
prefer to enter your data using a spreadsheet and export the result to
\R{}. \R{} provides a rudimentary interface to enter or modify the data, but
\R{} developers tend to focus on the real work instead of building
another spreadsheet application inside \R{}. So, you may want to use your
favorite spreadsheet application to enter your data, and import the
data into \R{} environment. Nevertheless, for the sake of exercise, we
will shortly introduce the difficult way. Later on, we will see that
actually understanding the way to handle data in \R{} may make your life
a lot easier. 

We will use the following table for most of the exercises in this
section.

\begin{center}
\begin{tabular}{lll}\hline
word &freq    & PoS \\\hline
the  &  69968 &DET \\
of   &  36410 &P \\
and  &  28850 &CNJ \\
a    &  23070 &DET \\
in   &  20866 &P \\
to   &  14995 &TO \\
to   &  11158 &P \\
is   &  10108 &V \\
was  &   9815 &V \\
he   &   9546 &PRO \\
\hline
\end{tabular}
\end{center}

\subsection{Creating a data frame on \R{} command line}

The table lists 10-most frequent words in well-known Brown corpus,
together with their frequency and part of speech. The part-of-speech
(PoS) tags displayed are a simplified tag set used in
\href{http://www.nltk.org/}{NLTK}. First, we would like to create a
data frame to store the table, and populate it with the first five
rows. 

\begin{Exercise}
Type the commands in the listing below (you can copy \& paste from the
listing). When presented, add the last 5 rows of the data into the data 
frame, and display the resulting table.
\end{Exercise}

\lstinputlisting[numbers=left]{listings/bc-short-edit.txt}

As you can see in line~4 of the above listing, if you type the name of
the data frame, \R{} presents you the value of the object as usual.  Note
the use of the assignment operator in the last command, otherwise
\lstinline{edit} will conveniently(!) display the resulting data frame
on the screen, but your changes will not be saved. You are probably
not impressed with \lstinline{edit()}, but it may come handy for
simple data editing needs.

\subsection{Importing data}

In this section we will work with a large data set.  You can also find
it on the web \url{http://www.let.rug.nl/~coltekin/R/data/brown.csv}.
The rest of the exercises assumes that you have downloaded this data
set to a directory (folder) named \lstinline{data} relative to the
directory \R{} is started. If not, you may need to adjust the path names
in some of the commands below.

\R{} can handle many data formats, including common spreadsheet
applications and other statistical packages or even database
management systems. However, as in our example data, the best choice
for interoperability is using text files. Conventionally, each line
stores a record (or case), and columns are separated using a
\emph{delimiter} character, or a separator. Common separators include
comma `\lstinline{,}', semicolon `\lstinline{;}' and \emph{tab}. You
typically know the delimiter because it is the one you choose while
creating the file. However, you can also figure it out by inspecting
the file, for example, using a text editor.

Now we are ready to read the data file. Before doing so, however, you are
encouraged to inspect the data file using a text editor, or try to
import it to a spreadsheet program (such as MS Excel) and see how it
looks. If you inspect the file, you will see that the file is not
`comma separated' as its extension suggests. Here is how to import the
file:

\lstinputlisting[numbers=left]{listings/dataframe-import.txt}

The commands on line~3 and 9 are familiar. They give some information
about each variable in the data set, and list the first 6 records,
respectively. \lstinline{read.table()} command needs some explanation.

For demonstration, the \lstinline+read.table()+ command above contains
some parameters that are either default, or not needed since \R{} detects
them automatically. We will go through them briefly. First parameter
is the name of the file. Nothing interesting here, except, in this
tutorial we use UNIX-like path separator slash (\lstinline+/+). The
examples with path names should work as it is on Windows as well as 
Linux, MacOS and other UNIX-like systems. However if you are tempted
to use usual Windows path separator, backslash (\lstinline{\}), you 
need to use double backslash (\lstinline+\\+). 

\begin{description}
\item[sep] is the separator character, in our case \emph{tab}. The
    \emph{tab} character is specified with special notation
    \lstinline{'\t'}, with which you may be familiar if you have any
    programming experience. 
    
    The extension (\lstinline{.csv}) stands for `comma separated
    value' but it is common to use either \emph{tab}, or
    \emph{semicolon} (\lstinline{;}) as separators, especially when
    comma is commonly used within the data.

\item[quote] specifies the quote mark. The records that contain special 
    characters, such as the
    separator are quoted when a data set is saved as a text file.
    Although our data file does not contain any quoted records, for
    demonstration, we specify double quotes (\lstinline{"}) as the 
    quote marker.
\item[header] controls whether the data file contains header row or not. 
    Our data file lists variable names on the first row. So, we signal 
    \R{} to take this row as header instead of a data record.

    Note that \R{} does not allow white space in column (variable) names.
    So, `\lstinline{freq}' or `\lstinline{frequency}' is fine, but
    `\lstinline{word frequency}' is not a good variable name in \R{}.  In
    \R{} world, it is customary to use dot `\lstinline{.}' instead of
    whitespace characters. So, if you want, you can use something like
    `\lstinline{frequency.of.occurrence}'.

\item[colClasses] specify the type of each variable (column) in our
    data set. This is particularly important for this data set, since,
    if not specified, \R{} converts non-numeric data to to factor
    (categorical) variables. We prefer to keep the \lstinline+word+ 
    and \lstinline+lemma+ fields as character strings. \R{} would correctly 
    identify \lstinline+frequency+ and \lstinline+pos+ fields even if not
    specified.
\end{description}

You should see the help text of  \lstinline{read.table()} if you need a
different setting. As you can see in the documentation, \R{} also offers
some shorthand functions with different defaults for commonly used
file formats, such as \lstinline{read.csv()} and
\lstinline{read.delim()}.

Writing data to a text file is similar, in case you'd like to get your
data out of \R{} environment in a portable format
\lstinline{write.table()} is what you are looking for.  We will
experiment with \lstinline{write.table()} it in an exercise.

In most cases you will not need more than \lstinline{read.table()} and
\lstinline{write.table()} offers. However, \R{} supports many external
file formats and more flexible methods of reading and writing data.
For example, if want to import data saved in another application such
as SPSS, you need to use functions defined in a library called
\lstinline{foreign}. We will discuss using libraries later in this
tutorial.

%To access functions in this library, type
%\begin{lstlisting}
%> library(foreign)
%\end{lstlisting} after this command you will have access to a number
%of new functions that start with \lstinline+read.+, such as
%\lstinline+read.spss()+.
%
%\R{} comes with many libraries for various tasks, and installing a
%library in \R{} is a relatively easy task, that we will return to this
%subject later. You can visit \url{http://cran.r-project.org/} and
%browse through the available packages, and read their documentation
%online.

\subsection{Accessing elements of a data frame}

We rarely use a data frame as a single object, we often need to
access or modify parts of your data frame. Here, we will briefly
exemplify some of the common tasks, and return to more advanced
manipulation of data frames in Section~\ref{sec:data-manip}.

Like vectors, you can access or modify individual elements of a data
frame. The only difference to keep in mind is that a data frame has
two index values. So, if you'd like to display the element in the
second row and third column of \lstinline{bc}, you'd write
\lstinline{bc[2,3]}. In general you can address any particular vale in
a data frame with the notation
\lstinline{dataframe_name[row_number,column_number]}. Indexes start
from 1 (If you wonder why such an obvious thing needs to be specified,
you can ignore the remark, you are in the right place). Instead of the
column number, you can use the name of the column,
\lstinline{bc[2,'freq']} is equivalent to \lstinline{bc[2,2]} for our
data set. Typically we do not give names to the rows, but in case you
have columns with name, you can also use the names as index values.

If you do not specify the row index, you get the specified column. For
example \lstinline{bc[,3]} will give you third column (PoS). Not
surprisingly, \lstinline{bc[2,]} will give you the complete second
row. Yes, you need to have the comma `\lstinline{,}' in both cases,
and for now, we will ignore the subtle differences. 

One more way to access the columns is to use the \lstinline{$}%stopzone
operator. For example, \lstinline{bc$freq}%stopzone 
refers to the vector of values in the second column of the data frame
\lstinline{bc}. Similarly, you can create a new column by assigning a
vector of appropriate size to a new column name using the same
notation.

One last note: when you work on a data set for a long time, even a
short data frame name like \lstinline+bc+ may make you feel threatened
by repetitive strain injury. The function \lstinline+attach()+ allows
you to access the variable names in your data frame directly without
typing the name of the data frame.

The following listing gives you a few examples, and you will have
opportunities to try these in the exercises below.

\lstinputlisting[numbers=left]{listings/dataframe-access.txt}

\begin{Exercise}
Use a text editor or spreadsheet program to enter the following
data, save as a text (e.g., CSV) file, and import to \R{} using
\lstinline+read.table()+. Name the resulting data frame
\lstinline+bc.caps+.

The data contains top six high-frequency words in Brown corpus, with
counts of  the cases where the first letter was uppercase or
lowercase. 

\begin{center}
\begin{tabular}{lrr}\toprule
word & freq.lower & freq.upper \\
\toprule
the & 62710 & 7258 \\
of & 36078 & 332 \\
and & 27912 & 938\\
to & 25729 & 426 \\
in & 19533 & 1801 \\
a & 21878 & 1314 \\
\bottomrule
\end{tabular}
\end{center}
\begin{Answer}
The way you import the data depends on the way you prepared it (which
we will not cover here). My data looks like this:
\lstinputlisting{data/bc-caps.data}
The following command does the job for me, assuming the data file is
called \lstinline{bc-caps.data}.
\begin{lstlisting}
> bc.caps = read.table('bc-caps.data', sep=';', header=T)
\end{lstlisting}
\end{Answer}
\end{Exercise}

\begin{Exercise}
Add a new column to \lstinline{bc.caps} with \emph{total relative
frequency} using the fact that there are about a million (1,000,000)
words in the Brown corpus.
\begin{Answer}

\lstinputlisting{listings/ex-dataframe-newcol.txt}
This one-line listing assumes that you have \lstinline{attach}ed the
\lstinline{bc.caps} data frame.
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:dataframe-scatterp]
Plot a scatter plot of lowercase frequency against uppercase frequency.
\begin{Answer}

\begin{minipage}{0.5\linewidth}
\lstinputlisting{listings/ex-dataframe-scatterp.txt}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
\tikzinput[half]{plots/exercises/dataframe-scatterp}
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:dataframe-textp]
Plot a graph similar to Exercise~\ref{ex:dataframe-scatterp}. However, 
plot the words where the points appear.
\begin{Answer}

\begin{minipage}{0.5\linewidth}
\lstinputlisting{listings/ex-dataframe-textp.txt}
\lstinline{text()} simply plots the text in the vector given as the
third parameter on coordinates specified by fist and second. However,
like \lstinline{lines()} and \lstinline{points()}, it only draws on an
already existing plot. Since \lstinline{text()} does not start a new plot, the
\lstinline{plot()} in second line is a trick to get an empty plot with
sufficient space. 
\end{minipage}%
\begin{minipage}{0.5\linewidth}
\tikzinput[half]{plots/exercises/dataframe-textp}
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:dataframe-import]
The file \url{http://www.let.rug.nl/coltekin/R/data/rt.txt} contains
(hypothetical) reaction times for a lexical decision task for two
groups, one is labeled as \emph{control}, and the other is labeled as
\emph{patient}. 

\begin{enumerate}
\item Inspect the file format, and import it using
\lstinline{read.table()}. Use the variable name \lstinline{rt} for the
resulting data frame. (You may realize that this is not the way to
enter data for processing with \R{}, but wait until 
Exercise~\ref{ex:dataf-conversion} before fixing it.)
\item Display the number of columns (variables) and rows (data
points).
\item Plot box-and-whisker plots for both conditions side by side.
\item Inspect normality of both samples graphically, and numerically.
\item Apply the appropriate test that gives you whether patients
perform worse than the control group (remember that these are reaction
times, lower numbers are better).
\end{enumerate}

\begin{Answer}

\lstinputlisting[numbers=left]{listings/ex-dataframe-import.txt}

\begin{enumerate}
\item If you just click on the link, you will see that the datafile is
      a comma-separated (CSV) file with a header. We could also read
      it using \lstinline{read.table()}, but it is more convenient to
      use the function \lstinline{read.csv()}. Furthermore, line~1
      also demonstrates that instead of downloading the data file and
      referring to the local file, you can directly specify a URL.
\item Note that \lstinline{length()} of a data frame is the number of
    columns. To get the number of rows, we used the
    \lstinline{length()} function on one of the column vectors.
    The function \lstinline{str()} gives the both along with some 
    additional information about each column.

\item There is nothing special about the box plot, except we did not
    have to specify the vectors separately. If you specify a data
    frame to \lstinline{boxplot()} it will try to plot all of columns
    side by side, and it will also get the labels from the data frame.
    The resulting box plots are presented in the figure below.

\item Q-Q plots, presented in the figure below,
    shows an approximately normal behavior. We will return to
    \lstinline{par()} command used here later. You can try and see the
    effect for yourself. Shapiro-Wilk test
    also confirms that there is not enough evidence that samples are
    non-normal. Remember that null-hypothesis in SW test is that the
    sample is normally distributed.
\item These are two independent samples, and we used independent
    samples t-test for this purpose. Since we are interested in whether
    the patient group performs worse (higher RT) we specified the 
    appropriate one-tailed tests.
\end{enumerate}

\tikzinput{plots/exercises/dataframe-import}
\end{Answer}
\end{Exercise}

