\section{\label{sec:sstats}Simple data exploration and statistical tests}

For most exercises in this section, we will be using the 
data introduced in Section~\ref{sec:vector}.
If you have completed the previous
exercise, you should have a vector variable \lstinline+wc+ with
10 numbers. If you skipped these sections, you need to type the
following to follow the examples and exercises below.
\begin{lstlisting}[language={}]
> wc <- c(3497, 3495, 3456, 3506, 3503, 3515, 3486, 3504, 3541, 3469)
\end{lstlisting}
Remember the alternative assignment symbol (\lstinline+<-+) used above is
equivalent to \lstinline+=+. 

\subsection{\label{ssec:graph-intro}Graphical data exploration}

We studied basics of plotting using the \lstinline+plot()+ function.
However, in most cases, producing useful graphs using
\lstinline+plot()+ requires too much work. \R{} provides easy
plotting functions for most tasks you'd need. However, they behave
in a consistent manner, and can be combined and extended using the
basic graphing utilities.

For visualizing a single data set, one of the most useful graphs is a 
\emph{histogram}. To display the histogram of the word-count data, type
\begin{lstlisting}
> hist(wc)
\end{lstlisting}
Now the `graph window' should show the histogram of the data.
The histogram you should get is presented in Figure~\ref{fig:histbox}a.
For now we will try one more graph before going on with another simple
statistical procedure. A \emph{box-and-whisker plot}, or simply
\emph{box plot}, is another useful tool for
visualizing data. Even though it is more interesting to see the
box plots of multiple variables next to each other, the box plot of a
a single value is also a good way to visualize how the data is
distributed. To see the box plot, type
\begin{lstlisting}
> boxplot(wc)
\end{lstlisting}
In a box plot the center of the distribution is represented in a box
(25$^{\textrm{th}}$ to 75$^{\textrm{th}}$ percentile), by default 
\R{} draws whiskers extending at most 1.5 interquartile range (IQR). Outliers are indicated by dots outside this range. In
case there are no outliers, whiskers will indicate the complete range
of the data.  Note that the way whiskers and the outliers
are drawn can be customized. The result of the above
\lstinline+boxplot()+ command is presented in
Figure~\ref{fig:histbox}b.


\begin{figure}
\begin{center}
\tikzinput{plots/hist-box}
\end{center}
\caption{\label{fig:histbox}Example graphs: (a) histogram 
of the same data set, (b) box-and-whisker plot.}
\end{figure}

\subsection{\label{ssec:normtest}Testing for normality}

A large number of statistical procedures assume that
the data follows a particular distribution, most typically, the normal
distribution. Hence, often we would like to know whether the data we
are using follows a (roughly) normal distribution or not. The
following few examples will demonstrate some of these tests for
assessing the normality of data using \R{}.

If you know the shape of the normal distribution, the histogram we
plotted in Section~\ref{ssec:graph-intro} already gives some
indications about the normality of our data. A graphical
method of checking for normality is to plot the theoretical
distribution over the histogram. The following commands plots the
normal curve with the same mean and standard deviation over the
histogram we have plotted before. Type the following commands and
examine the resulting graph.

\begin{lstlisting}
> hist(wc, freq=F)
> ndist = dnorm(3000:4000, mean=mean(wc), sd=sd(wc))
> lines(3000:4000, ndist)
\end{lstlisting}

In three lines we have introduced quite a few new functions and
notation. First, unlike previous histogram in
Figure~\ref{fig:histbox}, we used the parameter \lstinline+freq=F+
to the \lstinline+hist()+ function. This instructs \lstinline+hist()+
to scale the counts such that the area occupied by the boxes is equal
to 1.
This makes comparison to a probability density function easier (more
on density function in Section~\ref{sec:probdist}). Second, the second
line in the listing creates a vector variable which holds the
theoretical value of the normal distribution with the calculated mean and
standard deviation of our data stored in the vector \lstinline+wc+
in the range 3000 to 4000. Finally, the last line plots these values. One
thing to note (again) is that instead of \lstinline+plot()+ we used
\lstinline+lines()+ which, instead of starting a new plot, draws the
requested lines on the existing graph. The graph you should obtain is
presented in Figure~\ref{fig:norm-graphs}a.

At this point you are encouraged to check the documentation provided
in the \R{} package for functions \lstinline+hist()+ using \lstinline+help()+ or
simply the shortcut `\lstinline+?+'.

Another graphical method for checking normality is to plot a
\emph{quantile-quantile plot}, or Q-Q plot. \R{} provides standard
functions to plot Q-Q plots. Try the following:

\begin{lstlisting}
> qqnorm(wc)
> qqline(wc)
\end{lstlisting}

If distribution is approximately normal, all points should be located
(approximately) on the line drawn by \lstinline+qqline()+. The
resulting graph is presented in Figure~\ref{fig:norm-graphs}b.  Note
that another, more generic, function \lstinline+qqplot()+ can be used
to produce Q-Q plots for checking the fit of the data to other
distributions, or checking whether two samples come from the same
distribution.

\begin{figure}
\begin{center}
\tikzinput{plots/norm-graphs}
\caption{\label{fig:norm-graphs}(a) Histogram of \lstinline+wc+ and the theoretical normal
curve plotted on the same graph. (b) Q-Q plot of \lstinline+wc+.}
\end{center}
\end{figure}
\begin{lstlisting}
\end{lstlisting}

\begin{Exercise}
Given the above graphical tests, is the word-count data normally
distributed?
\end{Exercise}

Although visual indications provided above are a nice way of examining
whether your data fits to a certain distribution, a formal test is
more appropriate in most cases. Here are two more ways of checking for
normality:

\lstinputlisting[language={}]{listings/norm-tests.txt}

The first test above tests for normality of the data with well-known
\emph{Shapiro-Wilk test}. The second test is an unorthodox use of
\emph{Kolmogorov-Smirnov test} (because we estimated the mean and
standard deviation of the distribution from the sample). Note also the
use of \lstinline+pnorm+ as the second parameter of
\lstinline+ks.test()+. Remember that you can use \lstinline+help()+
for checking other options you can use with these functions.

\begin{Exercise}
What does the above test results say? Do the tests agree?
\end{Exercise}

% SW test's null hypothesis is that the distribution is normal, and KS test null hypothesis is that the distributions are the same. In both cases the p-values are rather high, and we cannot reject the null hypothesis.

\begin{Exercise}
Create a vector variable \lstinline+w+, containing the following values
\begin{lstlisting}
174, 71, 34, 38, 52, 113, 374, 120, 100, 422, 140, 25, 93, 520, 233
\end{lstlisting}
\begin{Answer}
\lstinputlisting[language={}]{plots/w-data.R}
\end{Answer}
\end{Exercise}


\begin{Exercise}[label=ex:sstat-ln-histbox]
Plot the histogram and boxplot of the data stored in vector
\lstinline+w+. Is the data distributed normally? Are there
outliers?
\begin{Answer}
\lstinputlisting[language={}]{listings/ex-ln-histbox.txt}
\tikzinput{plots/exercises/ln-histbox}\\
Both graphs indicate that the distribution of the data points 
is heavily skewed, and rather unlikely to be coming from a normal
distribution.
\end{Answer}
\end{Exercise}


\begin{Exercise}
Inspect the normality of the same data set using the Q-Q plot (you can 
use \lstinline+qqplot()+ as well, but it's easier with 
\lstinline+qqnorm()+ since we are testing for normality). 
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-ln-qqnorm.txt}
The points diverge from the theoretical straight line a lot. So,
\lstinline+qqnorm()+ also agrees that the distribution is not normal.
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/ln-qqnorm}
\end{minipage}
\end{Answer}
\end{Exercise}


\begin{Exercise}[label=ex:sstat-ln-normtest]
Now do a more formal test of normality, using
\lstinline+shapiro.test()+ (and \lstinline+ks.test()+ if you wish).
Is your judgments about normality in the previous
exercises supported by the test result?
\begin{Answer}
\lstinputlisting[language={}]{listings/ex-ln-normtest.txt}

Null hypothesis in Shapiro-Wilk normality test is that the 
distribution is normal. Since we get a low p-value, it indicates
that the null hypothesis is unlikely, and we conclude that most likely
our sample is not drawn from a normal distribution.

Similarly, The null hypothesis in (one sample) Kolmogorov-Smirnov test is
that the sample is drawn from the specified distribution with the
given parameters. Note that we cannot reject the null hypothesis using
\lstinline{ks.test()}. There are a number of reasons, first, 
\lstinline{ks.test()} is more general, but not as sensitive as
Shapiro-Wilk normality test. If you are testing for normality, you
should be using \lstinline{shapiro.test()}. Furthermore, (as you'd
find out if you read the documentation of \lstinline{ks.test}) you
should not be estimating the parameters of the theoretical
distribution from the sample at hand. This will typically cause
rejecting null hypothesis more difficult.
\end{Answer}
\end{Exercise}

\begin{Exercise}
If you have worked with similar data before, you probably have
realized that the data looks a lot like \emph{log-normal}.

Use \lstinline+ks.test()+ to check whether the data comes from a
log-normal distribution with parameters \lstinline{meanlog=5}, and
\lstinline{sdlog=1}. 
\textbf{TIP}: You can supply the function \lstinline{plnorm()} as the
reference theoretical distribution function to \lstinline{ks.test()}. 
\begin{Answer}
\lstinputlisting[language={}]{listings/ex-ln-lnormtest.txt}
As in Exercise~\refAnswer{ex:sstat-ln-normtest}, we are 
is unable to reject the null hypothesis using \lstinline{ks.test()}.
However, this time the p-value is even bigger, and we did not estimate
the parameters from the sample. As a result our result is more
reliable.

You may also want to try test for `log-normality' using estimated
parameters.
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:sstat-ln-qqplot]
Use \lstinline+qqplot()+ function to produce a Q-Q plot 
for checking whether the data is distributed according to the 
log-normal distribution with parameters \lstinline{meanlog=5}, and
\lstinline{sdlog=1}. Note that \lstinline{qqline()} is only useful for
a normal Q-Q plot. Use \lstinline{abline()} to draw a reference line.
\textbf{TIP}: \lstinline+qqplot()+ requires two data sets to check if
the distribution of the datasets are similar. To obtain
\lstinline+N+ random data points that is distributed according to the
log-normal distribution with the parameters we are interested you can
use \lstinline+rlnorm(N, 5, 1)+. We will discuss this function further
in Section~\ref{sec:probdist}.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-ln-qqplot.txt}
Even though there is some divergence from the theoretical line (some
of this can be explained by nature of the log-normal distribution),
the plot does not look `too bad'. 
Since we assume both samples are distributed according to same distribution and
the same parameters we expect the reference line to have intercept=0 and 
slope=1 (You may want to think about why \lstinline{qqline()} does not
always draw a diagonal line like this).

Note that since \lstinline{rlnorm()} gives a \emph{random} sequence
your results may be somewhat different.
\end{minipage}
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/ln-qqplot}
\end{minipage}
\end{Answer}
\end{Exercise}


\begin{Exercise}
Because of the log-normal distribution's relation to the normal
distribution, you can use many techniques available for working with
the normal distribution after taking the \lstinline{log()} of the values
that come from a log-normal distribution. Create a new vector variable
\lstinline{lw}, which contains the logarithm of the values of the
vector \lstinline{w}, and repeat the exercises
\ref{ex:sstat-ln-histbox}--\ref{ex:sstat-ln-normtest} for this set of
values.
\begin{Answer}

Here is the listing of an \R{} session including all steps:
\lstinputlisting[language={}]{listings/ex-ln-norm.txt}
The first line assigns \lstinline{log(w)} to another vector
variable \lstinline{lw} for convenience. In the histogram, we do not
see a clear bell-shaped curve because of small number of data points.
However, if you compare this histogram to the one in
Exercise~\ref{ex:sstat-ln-histbox}, there is no clear marks of
non-normality (e.g., a skewed distribution). The same is also visible
on box plot. However, the normal Q-Q plot is a better graphical method
for checking for normality, and compared to the Q-Q plot in 
Exercise~\ref{ex:sstat-ln-qqplot}, the points fit better to the
straight line.

Our non-graphical test \lstinline{shapiro.test()} also indicates that
the null hypothesis (that the sample is drawn from a normal
distribution) is highly likely.

What we did in this exercise is log-transforming our data so that we
can use more sophisticated or better studied methods which are
applicable to normal data. This often comes handy in analysis of
linguistic (particularly corpus) data.

\tikzinput{plots/exercises/ln-norm}

\end{Answer}
\end{Exercise}
