source('config.R') #DONTSHOW#
plot(0, xlim=c(-10,10), ylim=c(-10,10), type='n')
abline(h=0) # x (horizontal) axis
abline(v=0) # y (vertical) axis
abline(0, 1,    col='red',   lty='solid')
abline(0, 2,    col='blue',  lty='solid')
abline(0, -1,   col='brown', lty='solid')
abline(2, -0.5, col='green', lty='dashed')
abline(-1, 2,   col='blue',  lty='dotdash')
abline(-1, -1,  col='brown', lty='dotdash')
# Fahrenheit - Celsius plot:
f <- runif(50, min=-10, max=70)
plot(f, -160/9 + 5/9 * f,
     xlim=c(-10,70), #alternatively: xlim=(9/5)*c(-40,40) + 32,
     ylim=c(-40, 40), 
     xlab='F', ylab='C',
     type='n')
abline(h=0, col='gray') # x (horizontal) axis
abline(v=0, col='gray') # y (vertical) axis
abline(-160/9, 5/9, col='blue')
