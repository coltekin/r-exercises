source('config.R') #DONTSHOW#
childes <- read.table('data/childes-example.data', head=T) #DONTSHOW#
childes$mot.mlu = childes$mot.mtok/childes$mot.utt #DONTSHOW#
childes$chi.mlu = childes$chi.mtok/childes$chi.utt #DONTSHOW#
childes$age.m = 25:48 #DONTSHOW#
childes$chi.ttr = childes$chi.mtyp / childes$chi.mtok #DONTSHOW#
m.full <- lm(chi.mlu ~ mot.mlu + age.m + chi.ttr, data=childes)
m.null <- lm(chi.mlu ~ 1, data=childes)
anova(m.full, m.null)
