plot(0, type='n', 
     xlim=c(0,10), ylim=c(0,10), 
     xlab='x', ylab='y',
     main='Title',
     cex.main=par('cex.main')/2, 
     cex.lab=par('cex.lab')*2)
text(0,10, "NW")
text(9.5, 0, "SE", cex=2)
points(5,5, pch=20)
points(5,5, cex=5.5)
