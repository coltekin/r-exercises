source('config.R') #DONTSHOW#
source('../plots/w-data.R')  #DONTSHOW#
lw <- log(w)
hist(lw)
boxplot(lw)
qqnorm(lw)
qqline(lw)
shapiro.test(lw)
