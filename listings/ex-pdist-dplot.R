source('config.R') #DONTSHOW#
x <- seq(-5, 5, by=0.1)
plot(x, dnorm(x), type='l')
lines(x, dt(x, df=3), col='red')
lines(x, dt(x, df=10), col='blue')
lines(x, dt(x, df=30), col='orange')
