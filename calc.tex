\section{\label{sec:calc}Simple calculations and variable assignment}

This section assumes that you are able to start and run commands in
\R{}.

\subsection{Using \R{} as a calculator}

\R{} can be used as a calculator. Try typing a few arithmetic
expressions at \R{}'s
prompt and check what happens. The listing below demonstrates some of
the arithmetic operations.

\lstinputlisting[numbers=left,language={}]{listings/calculator.txt}

The lines that do not start with a command prompt in the listing are
the outputs. \R{} can handle the standard arithmetic operators (addition,
subtraction, multiplication, division and power). In
line~5, the multiplication operation takes precedence: it is
calculated as \lstinline+6 - 12+, not \lstinline+3*4+. In line~7, to
make sure that addition is done before division, we used parentheses.
If you are familiar with usual operator precedence in programming
languages, \R{} will not surprise you.  However, there is no harm in
adding a couple of parentheses to make sure you get the result you
want.

Another thing to note in this listing is that \R{} regards any text after
the hash sign (\#) until the end of the line as a comment, and ignores
it. Comments do not have much use during interactive use, but they come
handy when you save command sequences (\R{} scripts or programs) in
files for future reference. 

\subsection{\label{ssec:variables}Variables}

Under the hood, \R{} provides a complete general purpose programming
language (in fact \R{} is an implementation of language \emph{SPLUS})
which may be really handy if you have some programming background. In
this set of exercises we will not go into programming. However, we
will be using variables frequently.

Use of variables may save you from quite some typing, and \R{} will save
the values of variables on exit by default so that you can access the
same values when you restart \R{}.

To assign a value to a variable you can use the assignment operator,
`\lstinline+=+', (or, equivalently, \lstinline+<-+ as real \R{} experts
do). And you can use the variables in calculations or if you type a
variable name and press enter, \R{} will report the value. Here is an
example that demonstrates the basic use of variables: 

\lstinputlisting[numbers=left,language={}]{listings/variables.txt}

In line~1 we store the
value \lstinline{2010} in variable \lstinline{now} (yes, now is
relative). In line~2 we used the alternative assignment operator
\lstinline{<-}, this is equivalent to \lstinline{=}. In this tutorial
we use both somewhat randomly to remind you that you may see \R{} code
using both, and they are equivalent (see the answer
of Exercise~\refAnswer{ex:calc-z}, for one more assignment operator).

In line~2 and 3 we use a dot `\lstinline{.}' instead of space. \R{}
variable names cannot contain space characters, and dot is the conventional
character instead of space in \R{} community. There are more rules for
variable names. For example, they cannot contain many other special
characters (like \lstinline{-}, \lstinline{+}, \lstinline{/}) and they
cannot start with numbers. 

Line~3 demonstrates use of character strings. Character strings must be
enclosed in matching double (\lstinline+"+) or single (\lstinline+'+)
quotes. \R{} supports a variety of operations on string type, and it may
come quite handy while working with language data (corpus). Apart from
numbers and strings there are other types that your variables can
take. For example \emph{boolean}s  that take values \lstinline+TRUE+
or \lstinline+FALSE+ and \emph{categorical}  variables (or
\emph{factor} variables as \R{} calls them) are interesting for many
statistical tasks. We will return to discussion of these types later.

Line~4 subtracts value of \lstinline{birth.year} from \lstinline{now}
and stores in a new variable \lstinline{age}. As demonstrated in
line~5, if we type the name of the variable \R{} tells us the value stored
in the variable.

Line~7 may be confusing for non-programmers. This line adds
\lstinline{2} to variable \lstinline{now}, and re-assigns the new
value to the same variable \lstinline{now}. In other words, we increment
\lstinline{now} by \lstinline{2}.

In line~8, we (re)calculate the age, but beware: the case matters in
variable names. \lstinline{Age} is not the same as \lstinline{age}. As
a result we have two variables now, lowercase \lstinline{age} still
contains the previous calculation on line~4, and uppercase
\lstinline{Age} contains the calculation in line~8. The rest of the
lines demonstrate this difference.

You should enter this command sequence in \R{} to check if all works as
in the listing.

If you'd like to see the user variables, you can use the function
\lstinline+ls()+, and if you want to get rid of one, for saving space
or any other reason, you can use \lstinline+rm()+.

\subsection*{Exercises}

\begin{Exercise}[label=ex:calc-z]
Perform the following actions in \R{}:
    \begin{enumerate}
        \item store 20 in the variable \lstinline+x+
        \item store 10 in the variable \lstinline+m+
        \item store  5 in the variable \lstinline+s+
        \item subtract \lstinline+m+ from \lstinline+x+, store the result in 
            \lstinline+t+
        \item divide \lstinline+t+ by \lstinline+s+, store the 
            result in variable \lstinline+z+
    \end{enumerate}
    What is the contents of variable \lstinline+z+?
\begin{Answer}

Here is a way to do it:
\begin{lstlisting}[numbers=left]
> x = 20
> m = 10
> 5 = s
> t <- x - m
> t / s -> z
> z
[1] 2
\end{lstlisting}
Note the different assignment operators, \lstinline{<-} and
\lstinline{->}, in lines 4 and 5. We have already mentioned
\lstinline{<-}. \lstinline{->} is similar, but you place the target
variable to the right of \lstinline{->}, which is sometime handy when
you start writing a complex expression and what to store the result in
a variable.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Redo the calculations in Exercise~\ref{ex:calc-z} steps 4 and 5
without the use of the temporary variable \lstinline+t+. Use parentheses
if necessary to get the same result.
\begin{Answer}
\begin{lstlisting}
z = (x - m) / s
\end{lstlisting}
Thing to note is that you need parentheses around the subtraction
operation for correct interpretation. What is the result without parentheses?
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:calc-log]
\R{} supports a wide range of mathematical functions. A function that is often useful in statistical analysis is
    the \emph{logarithm} function. Using the shortcut you learned in
    Section~\ref{sec:start} search for the function name that returns
    logarithm of a given number, and use it to calculate logarithm of
    \lstinline+2.7+.
\begin{Answer}

First we search using the keyword 'logarithm'

\begin{lstlisting}
> ??logarithm
base::log               Logarithms and Exponentials
nlme::logDet            Extract the Logarithm of the Determinant
\end{lstlisting}
Some additional information is excluded from this listing.
In this case \R{} finds two matches. We are interested in the first one.
The notation \lstinline{base::log} tels that there is a
\lstinline{log} function in package \lstinline{base}. We will study
use of packages later. It is reassuring that it is in
\lstinline{base} package. Which means it is directly accessible.

Now we know the function name, we calculate the logarithm of
\lstinline{2.7}:
\begin{lstlisting}
> log(2.7)
[1] 0.9932518
\end{lstlisting}
\end{Answer}
\end{Exercise}

\begin{Exercise}
You should have obtained a value close to \lstinline+1+ in
Exercise~\ref{ex:calc-log}.  This is because of
the fact that \R{} calculates \emph{natural logarithm} (base
\emph{e}=2.718282\ldots) by default. Often we use base-\lstinline+2+ logarithm.
The function you used above can be used to calculate
base-\lstinline+2+ logarithm as well. Using the shortcut you learned
in Section~\ref{sec:start} read the help for the logarithm function to
learn how to change the default base for the logarithm function. 
Calculate base-\lstinline+2+ logarithm of \lstinline+2.7+.
\begin{Answer}

If you have tried \lstinline{help(log)} or \lstinline{?log} (or
\lstinline{?base::log}), you see that you can give a second parameter
as base of the logarithm. Furthermore, \R{} gives you `shortcut'
functions for common bases like, \lstinline{log2()} and
\lstinline{log10()}. We will use the first method:
\begin{lstlisting}
> log(2.7, base=2)
[1] 1.432959
\end{lstlisting}
We could just type \lstinline{log(2.7, 2)}, since the value is the
first and \lstinline{base} is
the second argument. The notation we
use specifies the \lstinline{base} parameter explicitly, so that you
do not need to remember the order of the parameters. This notation is
particularly useful for well known parameters  (like
\lstinline{mean} or \lstinline{sd}) of other commonly used functions as well.
\end{Answer}
\end{Exercise}

\begin{Exercise}
After completing the exercises in this section, you should have 
a set of variables that we will not use in the future. List the
variables in your \R{} session, and delete the variables \lstinline+t+ 
and \lstinline+x+ (If you wish, you can delete all variables created in
this section. We will not use them again in the rest of the
tutorial.). List your variables again to see if you have achieved
the desired result.
\begin{Answer}
\begin{lstlisting}
> ls()
[1] "m"  "s" "t"  "x"  "z"
> rm(t,x)
> ls()
[1] "m"  "s" "z" 
\end{lstlisting}
Here is a tip for deleting all variables without needing to list all files
(use with caution!):
\begin{lstlisting}
> rm(list=ls())
> ls()
character(0)
\end{lstlisting}
\end{Answer}
\end{Exercise}
