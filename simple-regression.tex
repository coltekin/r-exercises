\section{\label{sec:sregression}Linear regression for single explanatory variable}

Linear regression is one of the most powerful tools in statistics. In
this section we will work on basic linear regression on \R{}.  We will
return to more complicated linear models in later sections. 

\begin{Exercise}[label=ex:lr-abline]
This is a refresher on the equation for 
a simple line on X-Y plane. You can safely skip it if you do not want
to be bothered with high-school mathematics. If you decide to go for
it, you will also get a bit of practice on graphs in \R{}.

We have already used \lstinline{abline()} function in a few occasions.
The \lstinline{ab} part of the function name comes from the fact that
conventional way to specify a straight line is the formula: 
\[ y = a + bx \] 
In this formula $y$ is the (dependent or response) variable we want to 
predict depending on the value of the (independent or explanatory) 
variable $x$. If we draw this line on the X-Y plane, we obtain a 
straight line where $a$ corresponds to the point that the line 
\emph{intercept}s the Y-axis. On the other hand, $b$ corresponds to 
the \lstinline{slope} of the line.


\begin{itemize}
\item Draw the following lines using \lstinline{abline()}.
Use the same line color for the lines with the same slope, 
and the same line type for the lines with the same intercept.\\
    \begin{itemize}
    \item[.] $y = x$
    \item[.] $y = 2x$
    \item[.] $y = -x$
    \item[.] $y = -0.5x$
    \item[.] $y = 2 + x$
    \item[.] $y = -1 + 2x$
    \item[.] $y = -1 - x$
    \end{itemize}
\textbf{TIP:} \lstinline{abline()} requires an already existing plot. You 
can use\\
\lstinline{plot(0, xlim=c(-10,10), ylim=c(-10,10), type='n')}\\
for initializing an empty graph.\\
\textbf{TIP2:} You can specify the line type using the option
\lstinline{lty}, with strings \lstinline{solid}, \lstinline{dashed},
\lstinline{dotted}, \lstinline{dotdash} etc., see help for
\lstinline{par} for details.
\item Two well-known temperature scales Celsius and Fahrenheit are
linearly related. Given a temperature measurement in Fahrenheit (F), you
can find the Celsius (C) value by,
\[ C = -\frac{160}{9} + \frac{5}{9} F \]
\begin{itemize}
\item What is the intercept and slope values in this equation?
\item Obtain a random sample of 50 numbers between -10 and 70.
Assuming these values correspond to Fahrenheit readings, calculate
corresponding Celsius values, and create a scatter plot of Fahrenheit
vs.\ Celsius. \textbf{TIP:} You can use \lstinline{runif()} to obtain
uniformly distributed random numbers.
\item Plot the line representing the relationship between Fahrenheit
and Celsius scales over the scatter plot.
\end{itemize}
\end{itemize}
\begin{Answer}

\lstinputlisting{listings/ex-sreg-abline.txt}
\tikzinput{plots/exercises/sreg-abline}
\end{Answer}
\end{Exercise}

As usual, we will not get into details here, but an additional bit of
refresher on regression may help you to follow the rest of the section
without consulting your favorite statistics textbook.  In
Exercise~\ref{ex:lr-abline} we examined completely deterministic
relations between two variables. In most research we are interested in
finding such deterministic rule-governed relationships. However, data
collected in real life does not line up as neatly as in our examples
above. The relationship between two variables we investigate generally
include a component that we cannot account for. To make this explicit,
we add a term to our linear equation as follows:

\[ y_{i} = a + bx_{i} + \epsilon_{i} \] 

The variable $\epsilon_{i}$ in this equation is called
\emph{residuals} which are typically due to measurement error or other
factors that we did/could not account for in the deterministic part of
our model. Finding the best equation that describes the data is what
linear regression is about. In simple linear regression, the best fit
is obtained by finding the intercept (\lstinline{a}) and slope
(\lstinline{\lstinline{b}) value that minimizes the sum
$\epsilon_{i}^{2}$ (\emph{least squares regression}). 

As usual, before starting the real work, we need to get our data into
\R{} environment.

\begin{Exercise}[label=ex:sreg-readdata]
The essay word counts and corresponding grades are stored in file
\url{http://www.let.rug.nl/coltekin/R/data/wcgrades.csv} as a 
comma-separated file. Load this datafile into a data frame called
\lstinline{wcgrades}, and display the result.
\begin{Answer}

\lstinputlisting{listings/ex-sreg-readcsv.txt}
\end{Answer}
\end{Exercise}

The rest of the discussion assumes that you completed
Exercise~\ref{ex:sreg-readdata}. You should have 10 data points, with
corresponding word count (\lstinline{wc}) and \lstinline{grade}
variables. We use the function \lstinline{lm()} to
fit a linear model. Here is how to do it (explanation of the
output follows the listing):

\lstinputlisting{listings/sreg-lmintro.txt}

First, you should have noted that the parameter to \lstinline{lm()}
has a somewhat unusual notation.  In \R{}, the symbol \emph{tilde}
`\lstinline{~}' defines a relationship two or more variables using a
\emph{formula}. In the simplest case above, the notation
\lstinline{grade ~ wc} means that the dependent variable
\lstinline{grade} is explained by the independent variable
\lstinline{wc}. This formula roughly corresponds to the simple linear
equation $y = a + bx$ which we would write as \lstinline{y ~ x} in
\R{}.
Second, \lstinline{lm()} reminds about what you typed, and it gives
the intercept and slope labeled as \lstinline{wc}, which means
that in conventional notation, the line can be described as
$grade = -170.23985 + 0.06998 \times wc$. Intercept in this case means
the expected grade for an essay of zero words, obviously not quite
useful. The slope, tels that for each word you add to your assay, your
grade will increase by $0.06998$. 


Apart from that, \lstinline{lm()} does not say much. What happens is
lm() fits the best model, and returns all the relevant data in a
`\emph{model object}'. However, the function prints out only the most
important information. For more, you need to ask more questions.
Often, you want to save the \emph{model object} returned by
\lstinline{lm()} into a variable, and ask further questions on this
object. Here is how to get a basic summary:

\lstinputlisting{listings/sreg-summary.txt}

The output of \lstinline{summary()} on the model object is more
similar to what you'd get from other statistical software. First bit
of interest is the summary of the \emph{residuals}. Remember that most
important assumptions of linear regression are concerning the
residuals. Namely, (1) the residuals are assumed to be independent and
(2) approximately normally distributed.  \R{} gives the five-point
summary as a first indication of how residuals are distributed. The
fact that the median is not very close to 0, and minimum being farther
away from the center compared to maximum is a bit alerting. We will
return to these concerns later. 

Not surprisingly, intercept and the slope (\lstinline{wc}) are the
same as before. However, the summary also includes t-test results for
both values. We are typically not concerned about the intercept (who
cares what grade an assay of zero words will get?).  The t-test for
the slope indicating a non-significant estimate, means that we may
have the relationship just by chance. 

In the last block, \R{} reports $R^{2}$ , which is the square of the
correlation coefficient $r$, and can be interpreted as the amount of
variance in the dependent variable (\lstinline{grade}) that can be
explained by the independent variable (\lstinline{wc}).  The
adjusted-$R^{2}$, sometimes noted as $\bar{R}^{2}$, is more
interesting in multiple regression case. We will return to it in
Section~\ref{sec:linear}. And, at last, \R{} also presents an F-test
result, which has the same significance value as the t-test for the
slope. This value will always be the same as the slope for linear
regression with single explanatory variable, we will see the
difference when we work on multiple regression.

\subsection{\label{ssec:lr-diag}Diagnostics}

The output above indicates that we cannot guess the grade from the
essay length. However, we did not check whether the assumptions of
linear regression was correct or not. The important assumptions of
simple least-squares regression are,
\begin{itemize}
\item The relationship between the variables of interest is
(approximately) linear
\item residuals are independently distributed
\item residuals have constant variance
\item residuals are normally distributed (around 0)
\end{itemize}

Besides these assumptions, linear regression is sensitive to
\emph{outliers}. 

The first diagnostic you should perform is scatter plot of the data
together with the regression line. The scatter plot of our example,
and the regression line is presented in Figure~\ref{fig:sreg-scatter}.

\begin{figure}
\centering
\begin{minipage}{0.45\linewidth}
\lstinputlisting{listings/sreg-scatter.txt}
\end{minipage}%
\begin{minipage}{0.45\linewidth}
\tikzinput[half]{plots/sreg-scatter}
\end{minipage}
\caption{\label{fig:sreg-scatter} Scatter plot and the fitted line for
essay word-counts and grades.}
\end{figure}

First, note that \lstinline{abline()} conveniently extracts the slope
and the intercept from the model object returned by \lstinline{lm()},
without us specifying the values we see on the output.
Figure~\ref{fig:sreg-scatter} also reveals that the regression line is
affected drastically with one of the points. Otherwise, the points seem to line
up nicely. Fixing the problem is left as an exercise
(Exercise~\ref{ex:sreg-outlier}), but a \textbf{disclaimer:} data is
artificially generated. Do not blame me if you get low grades for
your lengthy essays.

\begin{Exercise}[label=ex:sreg-outlier]
Remove the influential outlier from the data set above, and repeat the
regression analysis, and the plot presented in
Figure~\ref{fig:sreg-scatter} without the outlier.
\begin{Answer}

\begin{minipage}{0.65\linewidth}
\lstinputlisting[numbers=left,breaklines=false]{listings/ex-sreg-outlier.txt}
\end{minipage}%
\begin{minipage}[t]{0.45\linewidth}
\hspace{-3cm}\tikzinput[half]{plots/exercises/sreg-outlier}\\
\end{minipage}\\
Except the reminder of how to remove a row from a data frame on
line~1, you should have noted that now the regression line fits the 
data points better, residuals are centered closer to 0 with a more symmetrical
distribution, the slope and the intercept values are highly
significant (although we rarely are interested in the significance of
the intercept value), and $R^{2}$ tells us that we can almost
perfectly predict grades from the word counts (in case you missed the
disclaimer in the text: the data is artificially generated, you should
not write extend your essays unnecessarily to get higher grades).
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=anscombe]
The exercises so far should have already convinced that \emph{you
should always plot and visually inspect your data}. To emphasize that
we will experiment with a well-known data set that demonstrate
the effects of failing to visualize your data.
The file \url{http://www.let.rug.nl/coltekin/R/data/anscombe.csv}
contains data for a data set know as 
`\href{http://en.wikipedia.org/wiki/Anscombe%27s_quartet}%stopzone
{Anscombe's quartet'}. The file includes four pairs of $x$ and $y$ values,
labeled \lstinline{A.x}-\lstinline{A.y}, \lstinline{B.x}-\lstinline{B.y}, 
\lstinline{C.x}-\lstinline{C.y}, \lstinline{D.x}-\lstinline{D.y} in
this file.
\begin{itemize}
\item Read the data file.
\item Fit linear models using \lstinline{lm()} to each pair. Assign
    resulting model objects variables.
\item Create a scatter plot for each pair, and plot the fitted
    regression line over it. \textbf{TIP:} You can use the command
    \lstinline{par(mfrow=c(2,2)} to plot all four graphs on the same
    canvas.
\item Identify possible problems for each model.
\end{itemize}
\begin{Answer}

\tikzinput{plots/exercises/anscombe}

And here is the code for the plots
\lstinputlisting[numbers=left]{listings/anscombe.txt}

The answer of the last item is left as a real exercise.
\end{Answer}
\end{Exercise}


Most important assumptions of linear regression is about the way the
residuals (the error, or the variation that we cannot account for)
behaves. As a result, diagnostic tests for linear regression has a lot
to do with the residuals. Two functions that will help us check our
model's assumptions are are \lstinline{fitted()} and
\lstinline{resid()}. As their name suggests former gives the
predictions for each $x$ value in the data. In our case, this would be
the predicted grade points for each measured word count. The latter,
\lstinline{resid()}, gives you the residuals.

\begin{Exercise}
\begin{itemize}
\item For linear regression it is important that the residuals are
(approximately) normally distributed with mean 0. Extract the
residuals from the models fitted both with and without the outlier,
check whether residuals are normally distributed or not using Q-Q
plots and Shapiro-Wilk normality test.
\item What is the sum of residuals and the fitted values? Check your
answer by summing the fitted values and residuals.
\end{itemize}
\begin{Answer}

You can find the commands needed and the resulting Q-Q plots below.
We assume that the \lstinline{m} and \lstinline{m2} contain the models
fitted by \lstinline{lm()} with and without the outlier respectively.
The listing and the graphs produced can be found below. The normality
tests does not result in significant non-normality conclusion in both
cases. Plotting histograms may also help for checking the center and
the shape of the distribution.

The answer of second question can be answered as a direct consequence
of the definition of the residuals. The sum of fitted values and
residuals should be the observed values for the dependent variable (in
this case \lstinline{grade}). Lines 19 and 22 demonstrates that.

\lstinputlisting[numbers=left]{listings/ex-sreg-fitted-resid.txt}
\tikzinput{plots/exercises/sreg-resid}

\end{Answer}
\end{Exercise}

The exercises you did above are some of standard diagnostic tests you
should do for every linear regression model. Doing these tests
separately as we did above makes a good exercise for using \R{}. However,
\R{} provides an easier way to produce diagnostic graphs. To do this,
all we you need to do is to type (assuming your model is saved in
variable \lstinline{m})

\begin{lstlisting}
> plot(m)
\end{lstlisting}

This will produce four diagnostic plots below.

\tikzinput{plots/lr-diag}

All these graphs expose possible violations of modeling assumptions.
The top-left graph, \emph{residuals vs.\ fitted} gives an indication
of whether your residuals are correlated or not (independent). What
you do not want to see here is to have any sort of pattern. The graph
at the bottom left, \emph{fitted vs. $\sqrt{|standardized residuals|}$}
is basically the same graph, but with a different scale. It helps
seeing some patterns that are difficult to see in the first graph. In
our example, although it is difficult to decide due to small number of
data points, there seems to be an increasing pattern. This is an
indication of poor model fit (we already know that we have a very
influential outlier). The cases that may be causing the problems are
labeled (by their row number case in the data frame). This graphs
identify data points 3, 8 and 9 as potential causes of poor model fit.

The top-right graph is the familiar Q-Q plot. For simple linear
regression we want our residuals to be normally distributed. This
should show on Q-Q plot as points on a straight line. Some deviation
is OK, but we do not want to see a pattern. Again, it is difficult to
judge due to small number of cases, but our example seems to diverge
from the ideal line in both tails.  We could use
\lstinline{shapiro.test()} on \lstinline{resid(m)}, which extracts the
residuals from the model object \lstinline{m}.

The last, bottom-right graph gives an indication of influential data
points. The points that appear outside the contour lines drawn need
particular attention. The data point 9 is obviously too influential, 3
also needs some attention, and data point 6, although within the
contour lines, has more influence than others as well.


\subsection{Predictions of a linear model}

We mentioned at the beginning that a linear model helps us predict
value of a (dependent) variable from another (independent variable).
This suggests that, once we fit the model, we can predict the value of
the dependent variable that correspond to a particular value of the
independent variable. The predicted value is easy to guess, we know
the slope and the intercept. So, we can put any value into the linear
equation and find the result. The function \lstinline{predict()} does
that for us:
\lstinputlisting[numbers=left]{listings/sreg-predict1.txt}
Note that if you only specify the model,
\lstinline{predict()} will print the fitted values, which you could
also get using the function \lstinline{fitted()}. Specifying a new 
value may look a bit complicated. What we do in line~6 is to predict
the grade for a 3500-word essay. First, we put 3500 into a vector,
then we create a data frame with a column (variable) named
\lstinline{wc}, and pass this to predict as \lstinline{newdata}. The
need for this complication will be more clear when we work with
multiple regression later. But, soon we will also make use of the fact
that we can ask \lstinline{predict()} to predict multiple values at
once.

\lstinline{predict()} does more than only calculating the value
predicted by the regression line, it can also indicate reliability of
the predictions, by stating lower and upper confidence intervals for
the prediction. We will try to clarify it by going through the 
an example step by step.

\lstinputlisting{listings/sreg-predict2.txt}

We see two different intervals above specified using the \lstinline{int}
argument. The first one, \lstinline{confidence}, is a confidence
measure about how reliable the regression line itself is. With
more data points, the bounds of \lstinline{confidence} interval 
get closer to the predicted value. The second interval, 
\lstinline{prediction}, is where the 95\% of the data points should 
fall. This interval should not be affected 
by the number of observations that much. These intervals correspond to
\emph{standard error} and \emph{standard deviation}, respectively. 
The intervals can also be
visualized around regression line. Not all the details are explained
here, but Figure~\ref{fig:sreg-predline} presents the scatter plot,
regression line and two confidence bands for the data set
\lstinline{wcgrades} (before removing the outlier).

\begin{figure}
\centering
\begin{minipage}{0.45\linewidth}
\lstinputlisting[numbers=left]{listings/sreg-predict-line.txt}
\end{minipage}%
\begin{minipage}{0.45\linewidth}
\tikzinput[half]{plots/sreg-predict-line}
\end{minipage}
\caption{\label{fig:sreg-predline} Confidence (red) and prediction
(blue) bands for regression line.}
\end{figure}

\subsection*{Exercises}

\begin{Exercise}
Using the bc.all data set we worked with in Section~\ref{sec:data},
\begin{itemize}
\item Fit a linear model predicting rank of a word from it's 
    frequency. Transform the data if necessary.
\item Check whether the assumption of normality of the residuals with
0 mean is acceptable.
\item Check whether slope fitted by \lstinline{lm()} is significant.
\item Compare the $R^{2}$ with the correlation coefficient found in
    Exercise \ref{ex:cor-bc}.
\item Plot a scatter plot, the regression line and the confidence
bands around it.
\end{itemize}
\begin{Answer}

Most parts of this exercise is repetition of earlier exercises. The
full listing and the graphs can be found below. The listing below
log-transforms both variables. We remember earlier from
Exercise~\ref{ex:datafrmae-zipf} that, although not perfect,
log-transformation makes the variables almost linearly correlated. 
However, the plots to check for normality of the residuals does reveal
that the residuals are far from being normally distributed.

\lstinline{summary()} gives us significance values for the estimated
slope and intercept. Both are extremely significant. But you should be
very careful with this finding, since the necessary assumption
(normality of the residuals) is wrong. Furthermore, with so many data
points at hand, even a tiny slope could be statistically significant.
This is also reflected on the last plot, where both lines representing
the \lstinline{confidence} interval is drawn on the regression line.

One last remark: as expected the $R^2$ reported is the same as square
of the correlation coefficient (-0.9836312) we found in
Exercise~\ref{ex:cor-bc}.

\lstinputlisting[numbers=left]{listings/ex-sreg-freq-rank.txt}
%\SetTikzExport{png}
%\resizebox{\textwidth}{!}{
%\tikzinput{plots/exercises/sreg-freq-rank}
%}
%\ReSetTikzExport
\end{Answer}
\end{Exercise}

%\begin{Exercise}
%\end{Exercise}
%\begin{Answer}
%\end{Answer}
