\section{\label{sec:vector}Vectors in \R{}}

In Section~\ref{sec:calc}, we saw that we can use numeric values to do
simple calculations, and assign values in basic types (e.g., numeric
or string) to named variables.

In statistics, we are generally interested in a sample, or a list of
values. For that purpose, \R{} offers a data type called \emph{vector}.
Vectors in \R{} are similar to arrays or lists in programming languages.
The important thing to know is that a vector is a container of a set
of values of the \emph{same type}.

For the exercises in this section,  we will use the following data. For a class,
students are asked to submit a 3,500 to 4,000-word
report. 10 students turned in the reports with the following word
lengths:

\begin{lstlisting}[language={}]
3510,3508,3468,3520,3516,3525,3505,3519,3558,3487
\end{lstlisting}

To enter this data into a vector variable we type,

\begin{lstlisting}
> wc = c(3510,3508,3468,3520,3516,3525,3505,3519,3558,3487)
> wc
 [1] 3510 3508 3468 3520 3516 3525 3505 3519 3558 3487
\end{lstlisting}

This example demonstrates the primary way of assigning a vector to a
variable. The function \lstinline+c+ (stands for concatenate), puts
together its arguments into a vector. Like simple data types, if we
type the name of the variable, we get its value displayed. Entering
large datasets this way is, at best, cumbersome, and \R{} provides other
ways of entering data to which we will return later. 

At this point you should type the above assignment command to create
the vector \lstinline+wc+. We will use this data set in the next
few sections.

\R{} supports mathematical operations between vectors and the scalar
values and vectors and vectors. Standard  \R{} functions that normally
take a basic value can also take vectors as arguments, in which case
the function is applied to all members of the vector. 

Elements of a vector can be selected by specifying the position of the
element(s) between square brackets after their name. For example, if
we want to refer to the fourth element of vector \lstinline+wc+,
\lstinline+wc[4]+ (in fact, as we will see later, one can also
select possibly discontinuous ranges of data with this notation).

The following listing demonstrates some of these operations.

\lstinputlisting[numbers=left,language={}]{listings/vectorop.txt}

The first line multiplies a vector with a scalar value. In
other words, all members of the vector is multiplied with
\lstinline+2+. Line~4, on the other hand, sums two vectors. Finally,
in line~6, the function \lstinline+log()+ is applied to each member of
the resulting vector.

Besides the arithmetic operations and scalar functions applied to
vector elements, there are a set of functions that operate on vectors.
The following listing demonstrates some of these functions. Note that
the listing already includes a few statistical functions (finally we
are getting closer to the point!).

\lstinputlisting[numbers=left,language={}]{listings/vectorop2.txt}

\subsection*{Exercises}

\begin{Exercise}
Remember that the \emph{standard error of the mean} can be
calculated using the formula
\[
    \frac{s}{\sqrt{n}}
\]
where \emph{s} is the standard deviation of the sample, and \emph{n} is 
the size of the sample. Calculate the standard error of the mean for 
the word count data stored in \lstinline+wc+.
\begin{Answer}
\begin{lstlisting}
> sd(wc)/sqrt(length(wc))
[1] 7.485096
\end{lstlisting}
The \lstinline{sqrt(x)} function we used here is an alternative to 
\lstinline{x^0.5}.
\end{Answer}
\end{Exercise}

\begin{Exercise}
In Exercise~\ref{ex:calc-z} you calculated z-score for a single value
using pre-specified mean and standard deviation.
More formally z-score is calculated with the following formula:
\[ z = \frac{x - \mu}{\sigma} \]
Calculate z-scores of the values in the vector \lstinline{wc}, and
assign it to a new vector variable named \lstinline{zwc}. Display the
resulting vector, it's mean and the standard deviation.\\
\textbf{TIP:} you can calculate the standard deviation using the 
function \lstinline{sd()}.
\begin{Answer}
\begin{lstlisting}
> zwc <- (wc - mean(wc)) /sd(wc)
> zwc; mean(zwc); sd(zwc)
 [1] -0.06759625 -0.15209156 -1.84199776  0.35488030  0.18588968  0.56611858
 [7] -0.27883452  0.31263265  1.96029119 -1.03929231
[1] 3.852463e-15
[1] 1
\end{lstlisting}
On line~2, we specified multiple commands on a single line
using using semicolons, \lstinline{;}. You could, of course, run each
command on a separate line.
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:vec-sort]
In a (hypothetical) country, four political parties got 36,35,8 and 71 seats in 
the parliament with 150 seats. Sort the numbers of seats in reverse order, 
    with the largest element first and the smallest as last.
\begin{Answer}
\begin{lstlisting}
> sort(c(36,35,8,71), decreasing=T)
[1] 71 36 35  8
\end{lstlisting}
Another side note here: \lstinline{T} is a shorthand for
\lstinline{TRUE}. It is unlikely to make a difference for most
purposes, but \lstinline{TRUE} is a language reserved keyword, and
\lstinline{T} is just a convenience variable. If you are interested
the following proves the point:
\begin{lstlisting}
> T
[1] TRUE
> T = FALSE
> T
[1] FALSE
> TRUE = FALSE
Error in TRUE = FALSE : invalid (do_set) left-hand side to assignment
\end{lstlisting}
If you try these, do not forget to \lstinline{rm(T)} at the end of your
trial. Otherwise, at some point later, you may have a hard time 
understanding `why things stopped working as they used to be'.
\end{Answer}
\end{Exercise}


\begin{Exercise}
Use the seat counts in Exercise~\ref{ex:vec-sort} to calculate
the percentages of seats for each party. Use a single expression, and
do not hard code the number of seats in the parliament into your
expression. You may want to store the data in a variable for
convenience.
\begin{Answer}
\begin{lstlisting}[numbers=left]
> seats = c(36,35,8,71)
> (seats / sum(seats)) * 100
[1] 24.000000 23.333333  5.333333 47.333333
> round((sort(seats, dec=T) / sum(seats)) * 100, 2)
[1] 47.33 24.00 23.33  5.33
\end{lstlisting}
Line~3 gives the answer, but line~4 uses the sorted list and
rounds the result to two significant digits after dot, which 
makes it easier to read.
\end{Answer}
\end{Exercise}

\begin{Exercise}
You realized that the word counts we used in this section 
    included the essay title and the student's name by mistake. For
    each essay, we would like to
    discount word counts by \lstinline{6,8,6,5,7,5,9,7,10,9}. 
    Store these values in a new
    vector variable \lstinline+wdiff+.
\begin{Answer}
\begin{lstlisting}
> wdiff <- c(6,8,6,5,7,5,9,7,10,9)
\end{lstlisting}
\end{Answer}
\end{Exercise}

    
\begin{Exercise}
Create a backup copy of the data stored in the vector
\lstinline+wc+ in the vector \lstinline+wc2+ (this may sound like
serious work, but you can simply use the assignment operator). Subtract
the vector \lstinline+wdiff+ from the vector \lstinline+wc+ and
store the result again in the vector \lstinline+wc+. Display the
contents of \lstinline{wc} and \lstinline{wc2}.
\begin{Answer}
\begin{lstlisting}
> /Simpwc2 = wc
> wc = wc - wdiff
> wc; wc2
 [1] 3497 3495 3456 3506 3503 3515 3486 3504 3541 3469
 [1] 3510 3508 3468 3520 3516 3525 3505 3519 3558 3487
\end{lstlisting}
Note, again, the use of multiple commands on the same line. This 
time it is particularly useful, since it allow easier comparisons 
of the (short) vectors.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Find the differences of \emph{mean}s of the values stored in
    \lstinline+wc2+ and \lstinline+wc+. Is it the same as mean
    of the vector \lstinline+wdiff+? (In other words is the difference
\begin{Answer}
\begin{lstlisting}
> mean(wdiff)
[1] 7.2
> mean(wc2) - mean(wc)
[1] 7.2
\end{lstlisting}
\end{Answer}
    of the means the mean of the differences?)
\end{Exercise}
