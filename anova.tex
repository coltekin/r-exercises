\section{\label{sec:anova}One-way ANOVA}

In Section~\ref{sec:twomeans} we studied comparing two means using
\lstinline{t.test()} and it's non-parametric counterpart
\lstinline{wilcox.test()}. In essence, these tests compare whether two
samples (or one sample and a known mean value) are different. In some
cases, we would like to know whether there are differences between
more than two samples. We can of course do multiple tests using, for
example, \lstinline{t.test()}. However, each tests we perform
increases our chance of finding a significance difference by chance.
ANOVA allows us to find out whether there are \textbf{any} differences
among any of the groups we study without running the risk of finding a
significant difference by chance. 

If I was not able to convince you about the multiple comparison
problem with this dry introduction, you should read your favorite
statistics textbook, or better read
\href{http://mindhacks.com/2009/09/18/scientists-find-area-responsible-for-emotion-in-dead-fish/}{this}
or \href{http://www.wired.com/wiredscience/2009/09/fmrisalmon/}{this}
popular science article about an experiment where researchers found
emotional changes in a dead salmon using fMRI (well, for different
stimuli of course).


One-way ANOVA is applicable when you have a numeric response variable
and a single categorical explanatory variable. ANOVA is useful when
you have more than two levels (e.g., groupings) of the explanatory
variable. To give ANOVA's affinity to t-test, we will first start with 
an explanatory variable with only two levels.

\begin{Exercise}
In Exercise~\ref{ex:dataf-conversion} you prepared a hypothetical
reaction time data. A version of this data can be found at
\url{http://www.let.rug.nl/coltekin/R/data/rt2.csv} as a CSV file.
Load this file to a data frame named \lstinline{rt2}.
\begin{Answer}

\begin{lstlisting}
rt2 = read.csv('http://www.let.rug.nl/coltekin/R/data/rt2.csv')
\end{lstlisting}
\end{Answer}
\end{Exercise}

The dataset contains two variables,
\lstinline{reaction.time} and \lstinline{condition}, and
\lstinline{condition} had only two levels. As a result, if we
are interested in finding a difference in this data set, what we can
do is to run a \lstinline{t.test()}. We do a t-test in the listing
below, but we will use an alternative notation.
\lstinputlisting{listings/anova-ttest.txt}
Instead of specifying two samples to \lstinline{t.test()}, we used the
formula notation. Remember that we read this formula as
`\lstinline{reaction.time} is explained by \lstinline{condition}'. As in
Exercise~\ref{ex:dataframe-import} where we did a one-tailed test, we 
see that there is no significant difference between the samples. Note
that we skipped the one-tailed specification, since we'd like to
compare the results with ANOVA, and for ANOVA the direction of the
difference does not make sense.

\lstinputlisting{listings/anova-twosamples.txt}

First, a quick pick at the p-value shows that indeed the result is
equivalent to the t-test performed earlier. More interestingly, 
we wrapped the formula into \lstinline{lm()}, first fitting a linear
model, and then asking for the ANOVA table of this model via
\lstinline{anova()} command. We will not go through the relationship
between linear models and ANOVA yet. For now, it suffices to note that
we did indeed fit a linear model. The function \lstinline{anova()}
only presents the results of the linear regression in a different way.
The rest of the table is rather straightforward to read.
\lstinline{anova()}, we can see that an F-tests was performed with
degrees of freedom 1 (we have two groups) and 58 (we have 60 data
points), and we found the same p-value the t-test indicated.

\begin{Exercise}
For the rest of this section, we will use a modified version of the
data set \lstinline{rt2}, not surprisingly, you can find it at
\url{http://www.let.rug.nl/coltekin/R/data/rt3.txt}, but note that
this file is separated with semicolon `\lstinline{;}'. Read this data
set to a data frame called \lstinline{rt3}.
\begin{Answer}

\begin{lstlisting}
rt3 = read.table('http://www.let.rug.nl/coltekin/R/data/rt3.txt', sep=';')
\end{lstlisting}
\end{Answer}
\end{Exercise}

The data set \lstinline{rt3} contains three groups for
\lstinline{control}, we assume that the group \lstinline{patient2} is
another group of patients in a later stage. Repeating ANOVA for this
data set is easy. However, we would like to have a look at our
data.

\begin{Exercise}
Draw a side-by-side box plot of the reaction time for each condition.
\begin{Answer}

\begin{minipage}{0.5\linewidth}
\lstinputlisting{listings/ex-anova-boxplot.txt}

There is nothing new in this command, except the demonstration of the
formula notation for \lstinline{boxplot()} command.
\end{minipage}%
\begin{minipage}{0.5\linewidth}
\tikzinput[half]{plots/exercises/anova-boxplot}
\end{minipage}
\end{Answer}
\end{Exercise}

The command for ANOVA is the same as before, except we get a bit
different output.

\lstinputlisting{listings/anova-3samples.txt}
We get the familiar report. Except, this time we have a significant
difference with p=0.03186. Note that both that 
DF values are different from the two-sample case.

The above result says that there is a difference among three different
groups, but it does not tell us where the difference is. In other
words, the above result indicates that at least one of the
following is true,
\[\begin{array}{ll}
\mu_{control} &\ne \mu_{patient}\\
\mu_{control} &\ne \mu_{patient2}\\
\mu_{patient} &\ne \mu_{patient2}\\
\end{array}\]
in most cases we also want to know the difference lies. Ideally, you
should have specific hypotheses to test even before you have started
to collect your data. However, there is no harm in exploring possible
differences that you have not considered before, as long as you are
careful about the problem with \emph{multiple comparisons}.

You can compare all pairs of group means using
\lstinline{pairwise.t.test()}. This function compares all means, and
makes necessary adjustments to the p-values such that getting a
significant result by chance due to multiple comparisons is
eliminated. Here is how to do it:
\lstinputlisting{listings/anova-pairwise-test.txt} The command simply
takes the (response) variable as the first argument and the `grouping'
variable as the second. Additional arguments you give will be passed
to \lstinline{t.test()}, for example,
\lstinline{alternative='greater'} would cause a series of one-tailed
tests to be performed.

The important part of the output is the pairwise comparison matrix,
where we see that only significant (at level 0.5) difference is
between \lstinline{control} and the \lstinline{patient2}. \R{} also
reports us that the p-values are adjusted using the method
\lstinline{holm}. You have probably heard of \emph{Bonferroni}, which
you can instruct \lstinline{pairwise.t.test()} to use by argument
\lstinline{p.adj='bonferroni'}. However, Bonferroni correction is
rather (too) conservative. The default adjustment method used by \R{},
\emph{Holm}, is conservative enough not to cause any hidden Type I
errors, but it is also more powerful so that it wouldn't cause too
many Type II errors.  There are a few more powerful methods methods
that you can use if certain conditions are met. Unless you know what
you are doing, probably you should stick to the default safe option.

\begin{Exercise}
Remember that we expect the reaction times in patients to be higher. 
Rerun \lstinline{pairwise.t.test()} to obtain a one-tailed test
reflecting this expectation.
\begin{Answer}

\lstinputlisting{listings/anova-pairwise-one-tail.txt}
Not surprisingly, we find more significant, but still the only
significant finding is the same.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Run \lstinline{pairwise.t.test()} such that the p-values are adjusted
according to Bonferroni correction, and compare the result with the
Holm correction shown above.
\begin{Answer}

\lstinputlisting{listings/anova-pairwise-bonferroni.txt}
Note that the most significant result is not changed. The other
p-values are higher compared to the Holm correction.
\end{Answer}
\end{Exercise}

\begin{Exercise}
You can also do pairwise comparisons if normality assumptions fail.
Similar to single comparison case, the function doing this in \R{} is
named \lstinline{pairwise.wilcox.test()}. Make a non-parametric
pairwise tests for the same data.
\begin{Answer}

\lstinputlisting{listings/anova-pairwise-wilcox.txt}
\end{Answer}
\end{Exercise}
