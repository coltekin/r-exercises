\section{\label{app:formula}Model formulas}

Various commands in \R{} accept a notation called \emph{model formula},
or simply \emph{formula}. The simplest form of the formula is,
\begin{lstlisting}
y ~ x
\end{lstlisting}
where \lstinline{x} and \lstinline{y} are two variables. You can read
this as `\lstinline{y} is explained by \lstinline{x}'. The
\emph{dependent} or \emph{response} variable goes to the left of the
tilde `\lstinline{~}' and the \emph{explanatory} or \emph{independent}
variables goes to the right. This formula roughly corresponds to the 
linear equation,
\[ y = ax + b\]

The interpretation is slightly different if the variables are
categorical. Note that the intercept, $b$, is implicit in the 
model formula. If you like, you can be explicit by using the notation
\lstinline{+ 1}. Or if you want to exclude it, e.g., force a
regression line passing through origin, you can exclude it by notation 
\lstinline{- 1}. In case you have multiple explanatory variables, it
is easy to include them using the same notation. For example if you
had two explanatory variables \lstinline{x1} and \lstinline{x2}, you
can specify it like this:
\begin{lstlisting}
y ~ x1 + x2
\end{lstlisting}
The linear equation that correspond to this notation would be 
$y = a_{1}x_{1} + a_{2}x_{2} + b$.

As you may have figured out already, the arithmetic operators such as
\lstinline{+} and \lstinline{-} have different meanings in a formula.
So, if the variable you are interested is a combination of \R{}
variables, then you need a special notation. For example, you might be
interested in fitting a linear model where \lstinline{y} is explained
by the sum of \lstinline{x1} and \lstinline{x2}. That is, the equation
you want to describe is $y = a \times (x_{1} + x_{2}) + b$. In such
cases you need to use a special function, \lstinline{I()}, to protect
the arithmetic operation from being interpreted as part of the
formula. In the case of our example, the correct formula notation is
\begin{lstlisting}
y ~ I(x1 + x2)
\end{lstlisting}

If your explanatory variables are categorical, as in ANOVA, you may
fit a model where interaction of the variables is important.
Interaction of variables in a formula is expressed with a term where
variable names are concatenated with column(s) between the variables. For
example, the formula
\begin{lstlisting}
y ~ x1 + x2 + x1:x2
\end{lstlisting}
expresses a model where interaction of \lstinline{x1} and
\lstinline{x2} are also included in the model fitting. For two
variables, we have only one possible interaction. If you have many
variables, and want to include all interaction terms, it may be a
hassle to type all the interaction terms separately. For example, all
interactions of three variables \lstinline{x1}, \lstinline{x2} and 
\lstinline{x3} consist of the two-way interactions 
\lstinline{x1:x2}, \lstinline{x1:x3}, \lstinline{x2:x3} and the 
three way interaction \lstinline{x1:x2:x3}. To include all
interactions, you can use `\lstinline{*}' instead of `\lstinline{+}'.
For example, to include three variables and all interactions in a
model formula, we simply type \lstinline{y ~ x1 * x2 * x3}.

The formula notation is quite flexible and can express many other
forms of `models'. The above explanation should be enough to get you
started. \R{} documentation you can find on CRAN is the main reference,
and you can find further information in the books and documents listed
in Resources section.
