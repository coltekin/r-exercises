\section{\label{sec:means-nonp}Non-parametric tests for comparing means}

We learned how to compare two means in Section~\ref{sec:twomeans} using
\lstinline{t.test()}. One of the assumptions of the t-test is that the
population the samples we draw from are distributed (approximately)
normally. In most cases we have reasons to believe that this
assumption holds, but in case we have suspicions that it does not we
have to resort to \emph{non-parametric} tests. If we do a t-tests
despite the violation of its assumptions, we may wrongly conclude that
we have a significant finding (so-called Type-I error). The
non-parametric tests, on the other hand, are more general. They do not
require the same (more strict) assumptions. On the downside, they do
not have the equivalent power either. We have a bigger chance of
missing a real significant effect with a non-parametric test. In this
section we will briefly work with non-parametric tests for the
comparing means in \R{}.

All t-test scenarios we have discussed in Section~\ref{sec:twomeans}
assume that the data is normally distributed. Often, we cannot justify
this assumption. We saw how to check whether normality assumptions are
warranted or not in Section~\ref{ssec:normtest}. For this section
we will assume that data sets \lstinline{wc} and \lstinline{wc2} we
used earlier are not normally distributed. If you do not already have
the data in your \R{} environment, you can get started by typing:

\begin{lstlisting}
> wc = c(3497, 3495, 3456, 3506, 3503, 3515, 3486, 3504, 3541, 3469)
> wc2 = c(3541, 3463, 3487, 3838, 3870, 3700, 3871, 3977, 3960, 3469)
\end{lstlisting}

\begin{Exercise}
We already checked the normality of \lstinline{wc} in
Section~\ref{ssec:normtest}.  Using the same techniques, you should check
whether \lstinline{wc2} is normally distributed or not.
\end{Exercise}

In \R{}, all non-parametric tests for comparing two means are collected
under the function \lstinline{wilcox.test()}. We will only give the
one-sample test as an example. Fortunately, the options to
\lstinline{wilcox.test()} are similar to \lstinline{t.test()}, and you
can simply copy and paste the arguments for a particular case. Note
that the function name \lstinline{wilcox.test()} may be confusing,
since, for example, two-sample test \R{} performs with this function is
commonly called \emph{Mann-Witney U-test}. You should read the
documentation of \lstinline{wilcox.test()} for further information on
particular tests implemented in \R{} for each case. Here is how
one-sample test is performed.

\lstinputlisting[language={}]{listings/wilcox-test.txt}

Since the effect size (the differences between the means
compared) is big, we get a highly significant result with even with
this non-parametric test. Nevertheless, compared to
\lstinline+t.test()+, the p-value is far bigger (the result
is less significant).

\begin{Exercise}
Perform a non-parametric test for comparing means of \lstinline{wc}
and \lstinline{wc2}. Compare the results of non-parametric test with
the results of t-test obtained earlier. If we were certain that our
data is normally distributed, what kind of error may the
non-parametric test cause?
\begin{Answer}

\lstinputlisting[language={}]{listings/two-sample-wilcox.txt}
As expected the p-value is higher. If we were using this test for
normally distributed data instead of t-test, we would run the risk of
not detecting an actual difference, making a type-II error.

You should also receive a warning message that exact p-value is
not computed. This is fine for most cases, but you may want to check
the documentation for more insight on what is going wrong and how much.
\end{Answer}
\end{Exercise}

\begin{Exercise}
Perform a non-parametric test for comparing means of \lstinline{wc}
and \lstinline{wc2}, assuming that the data is paired. Compare the 
results with the t-test with the same condition, and non-parametric
test with non-paired condition.
\begin{Answer}

\lstinputlisting[language={}]{listings/paired-wilcox.txt}
\end{Answer}
\end{Exercise}

\begin{Exercise}
Plot box plots of the data sets, \lstinline{wc} and \lstinline{wc2} we
used in this section side-by side. Does the graph indicate what you
found by formal tests about difference between these two means?
\begin{Answer}

\begin{minipage}{0.5\textwidth}

\lstinputlisting[language={}]{listings/ex-means-boxplot.txt}
The plot shows that the centers and the variances of the data 
sets are different. It also shows that there is some overlap, but in
general central tendency of the distributions seems to be different.
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/means-boxplot}
\end{minipage}


\end{Answer}
\end{Exercise}
