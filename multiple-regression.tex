\section{\label{sec:linear}Multiple regression}

In Section~\ref{sec:sregression} we experimented with simple linear
models where we had only a single explanatory variable. This section
builds on this by introducing cases where we have multiple. Although
we extend simple linear regression with multiple variables here, 
we will still stay on the surface. 

As usual, your first exercise is to prepare the data.

\subsection{Data preparation}

\begin{Exercise}
Load the data file
\href{http://www.let.rug.nl/~coltekin/R/data/childes-example.data}{childes-example.data}
in a data frame named \lstinline{childes}. The file is a
\emph{tab}-separated text file with the following variables from
recordings of mother--child interactions:

\begin{description}
\item[age] Age of the child during the relevant recording session(s).
The age follows a common notation used in child language acquisition
literature, where it is indicated using \lstinline{year;month}. For 
example, \lstinline{3;1} means three years and one months after the 
child's birth.
\item[chi.utt] The number of utterances recorded for the child during the session(s).
\item[chi.mtok] The number of morphemes (tokens) for the child in the session(s).
\item[chi.mtyp] The number of unique morphemes (types) for the child in the session(s).
\item[mot.utt, mot.mtok, mot.mtyp] The same as chi.utt, chi.mtok and
chi.mtyp, respectively, but for the mother's utterances.
\end{description}

Although the data is from real transcriptions of child--mother
interactions, it is simplified, and it is taken from recordings of a
single child. As a result, you should be careful about
interpretations of the results below, but it will serve well for the
exercises.
Inspect your data, e.g., by using \lstinline{str()},
\lstinline{summary()} and displaying it on the screen.
\begin{Answer}
\lstinputlisting[numbers=left,language={}]{listings/ex-regression-data.txt}
\end{Answer}
\end{Exercise}

\begin{Exercise}
A measure of children's level of language proficiency is called \emph{mean
length of utterance} (MLU), which is defined as the average number of
morphemes per utterance.

Add two new variables to the data frame, \lstinline{chi.mlu} and
\lstinline{mot.mlu}, containing the calculated MLU value of the child
and the mother respectively for each row.
\begin{Answer}
\lstinputlisting[numbers=left,language={}]{listings/ex-regression-mlu.txt}
\end{Answer}
\end{Exercise}


\begin{Exercise}
\begin{itemize}
\item Another variable of interest is called type-token ratio (TTR). Add a new variable \lstinline{chi.ttr} to \lstinline{childes}
data frame, which should be the ratio of morpheme types to morpheme tokens for
each row.
\item Because of the way it is specified in the data file, the variable \lstinline{age} is likely to be
taken as a \emph{factor} variable by \R{}. Convert \lstinline{age}
variable to a numeric variable, the age of the child in months, and
store it in a new column \lstinline{age.m}.
(\textbf{TIP:} you do not need to do any calculations, you can 
use \lstinline{seq()} (or equivalently the notation \lstinline{x:y}). 
The range includes all 24 months after second birthday of the child).
\end{itemize}
\begin{Answer}
\lstinputlisting{listings/ex-lr-age.txt}
We used the fact that the sessions started when the child was
24-months old, and continued every month until the month before the
4th birthday. A more complicated, but more flexible way to do it is,
\begin{lstlisting}
childes$age.m <- as.integer(substr(childes$age, 1,1)) * 12 +
                 as.integer(substr(childes$age, 3,5))
\end{lstlisting}
%stopzone
The solution above takes first character, the year, multiplies
it with 12, then adds the month which is extracted as the last two
characters of the string. It relies on the fact that year is always
one and month is always two characters long. An even more flexible 
solution can be found using \lstinline{strsplit()} function.
\end{Answer}
\end{Exercise}

\subsection{A refresher on regression}

Now we are ready to do some model fitting. We will start with a
refresher on regression with single explanatory variable.

\begin{Exercise}[label=ex:lr-simple-reg]
It is said that caregivers adjust their language complexity according
to the child's language abilities. Assuming that MLU is a good
indicator for both language ability and language complexity, fit a
simple linear model to test this assumption. Investigate the fit of
the model,

\begin{itemize}
\item with \lstinline{summary()}
\item by plotting a scatter plot of the data together with the fitted
    regression line.
\item by plotting the diagnostic graphs.
\end{itemize}
\textbf{TIP:} We worked through all these steps in
Section~\ref{sec:sregression}.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-regression-simple-reg.txt}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
\tikzinput[half]{plots/exercises/regression-simple-reg}
\vfill
\end{minipage}
\end{Answer}
\end{Exercise}


\begin{Exercise}[label=ex:lr-simple-influence]
Following the example above, fit another model, which does not include 
data points 1, 17, 22, and compare with the original model, 
\lstinline{m}, you fitted in Exercise~\ref{ex:lr-simple-reg}.
\begin{Answer}

\begin{minipage}[t]{0.5\textwidth}
The following listing only shows the summary of the model without data
points 1, 17 and 22. You should compare it the output obtained in the
answer of Exercise~\refAnswer{ex:lr-simple-reg}. One thing to note is
that the slope of the regression line is now smaller, indicating a
weaker relation between the two variables. This is also evident from
the slight drop on the $R^{2}$. We also lost some statistical
significance since we decreased the number of the data points.

The plot shows all data points, the previous full regression line with
solid black line, and the regression line with removed data points
with dashed blue line. The data points we removed from the second
model are shown in red. We will see further examples of such tricks on
graphs in Section~\ref{sec:graph}. For now, you should note that two
of the influential data points (the bottom-left and top-right points)
we have removed has been influential on increasing the slope. As a
result, removing these points caused \lstinline{lm()} to fit a line
with smaller slope.

\lstinputlisting[language={}]{listings/ex-regression-simple-influence.txt}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
\raisebox{-85mm}{%
\tikzinput[half]{plots/exercises/regression-simple-influence}%
}
\vfill
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:lr-simple-reverse]
Fit (yet) another model, this time reversing your dependent and
independent variables. That is, fit a model predicting the child's 
MLU from the mother's. Inspect your result using \lstinline{summary()}
and by plotting the fitted model over the scatter plot and also plotting 
the diagnostic graphs. 

** Before comparing the two models, try to answer whether the
following quantities will be the same or different for both models.
\begin{itemize}
\item slope and intercept values of the fitted model
\item $R^{2}$
\item influential data points
\item significance of the slope
\end{itemize}
\begin{Answer}
The commands you need to issue for this exercise are the same as
Exercise~\ref{ex:lr-simple-reg}. As a result, we will not go through
them. Regarding what changes:
\begin{itemize}
\item Since x-axis and y-axis will be reversed, we expect intercept
and slope to change. 
\item $R^{2}$ is the correlation coefficient, and it is symmetric. As
a result it will not change.
\item The influence of the individual data points will not be the
same. For example, a data point far away from others in the
x-dimension is likely to be influential, which is will not
necessarily have the same influence when axes are reversed. 
However, a significant overlap between influential data points 
is not unlikely.
\item  Since the slope
changes, you may expect its significance to change as well. 
However, the significance of the slope will be the same. Remember that
the significance value for the slope results from a t-test with null
hypothesis `the slope is equal to 0'. The mathematical proof of that
both regression lines should result in the same t value is rather
straightforward, but we do not scare you with proofs in this
tutorial. 

To get a sense of why this should be so, you can think in two
different ways. First, remember that the significance of the slope 
means that there is a significant correlation between the two
variables, and correlation is symmetric. Second, remember that t-test
actually works on standardized scores, and once the variables are
scaled to the same way, the slope should also be the same. 

You are encouraged to calculate z-scores for both values, fit two
models by exchanging the explanatory and the response variable using
the normalized scores, and do the comparison again: your slope should
be the same as well. If you do, you should also check the relation
between the $R^{2}$ and the slope value you found for the models
fitted using the normalized data.
\end{itemize}
\end{Answer}
\end{Exercise}


\subsection{Multiple regression}


Often, we would like to know the effect of multiple variables on our
response variable. We will study this by building on the simple
regression examples above.

We already saw that mothers' MLU was a predictor of the children's MLU
(causation seems to point the other way, though). We will take age as
another explanatory variable, and yet another measure \emph{type/token
ratio} (TTR) children's output. This is probably not quite a useful
research question, but we will try it as it demonstrates a few
concepts nicely. 

We add each additional explanatory variable to the right side of our
model formula, using a `\lstinline{+}'. Note that this plus sign does
not have the ordinary arithmetic summation (see
Appendix~\ref{app:formula} for more details on the notation).  We now
fit the model and have peek at the results. 

\lstinputlisting{listings/lr-mreg.txt}

Compared to simple linear regression, we now have multiple slope
values. As before, intercept is not really interesting (it is a good
exercise to think what it represents). First thing to note is that the
$R^2$ value became extremely high. With the additional variables, our
model fits the data very well. Unlike the simple regression the
`adjusted $R^2$' is not the same as the $R^2$. The $R^2$ is a positive
value. Adding new explanatory variables will always increase $R^2$,
even if the variable has no effect on the model fit. The adjustment in
the `adjusted $R^2$' subtracts this random effect from the $R^2$, as a
result it is a more credible indication of the model fit.

Now we turn to the effects of the individual explanatory variables.
Each slope value is an indication of estimated the effect of the
explanatory variables on the response variable. Interestingly, the
slope of the \lstinline{mot.mlu}, the variable we knew had a positive
correlation with our response variable is not negative, as well as the
child's own TTR, which we also expect to be positively correlated.
Both slope estimates are not statistically significant, however. The
reason behind this is \emph{covariance}.  That is, these variables
vary in a systematic way that their overall contribution is not
additive. Actually, if the effect was significant, we could interpret
the negative effect of \lstinline{mot.mlu} as `the mother increases
complexity of her utterances as child grows, but compared to the
effect of the child's age her adjustments fall behind the child's
language improvement'. We will return to discussion of covariance in
model selection soon. Before getting any conclusions, however, we need
to check whether assumptions of the regression modeling is correct or
not.  

The visualization of the data and fitted surface becomes difficult
with the increasing number of variables. However, if you give
\lstinline{plot()} a data frame argument, it plots a matrix of scatter
plots, which is generally useful to check for discovering
relationships between pairs of variables.

\begin{Exercise}
Produce a pairwise-scatter plot of all variables involved in the
example model above. Namely, \lstinline{mot.mlu}, \lstinline{chi.ttr},
\lstinline{age.m} and \lstinline{chi.mlu}.
\begin{Answer}
We do not show the graph here, but here is a way to plot only the
relevant variables:
\lstinputlisting{listings/ex-lr-scatter-multi.txt}
You could of course select the columns by index values as well.
\end{Answer}
\end{Exercise}

Although, the visualization of fitted surface and the data becomes
somewhat difficult, the diagnostic plots are the same. And, again you
should check whether model assumptions are violated or not by plotting
the four diagnostic plots we studied earlier in this section.

\begin{Exercise}[label=ex:lr-diag]
Plot the diagnostic graphs and  check whether modeling assumptions 
are violated or not. 
\begin{Answer}
Here are the diagnostic graphs. As before, \lstinline{plot(mm)} plots
these graphs. If you have doubts about how to interpret the graphs,
you should return to the explanation in Section~\ref{ssec:lr-diag}.

\tikzinput[half]{plots/exercises/lr-diag}
\end{Answer}
\end{Exercise}

\begin{Exercise}
As in Exercise~\ref{ex:lr-simple-influence}, compare your model with 
a model where most influential data point is removed.

\noindent\textbf{TIP:} if you cannot decide visually which data point
is the most influential one, you may want to check the functions
\lstinline{influence()} and \lstinline{influence.measures()}.
\begin{Answer}
The `residuals vs leverage' graph from Exercise~\ref{ex:lr-diag} 
indicates that the data points 1, 20 and 22 are more influential than
others. If you check the result with
\lstinline{influence.measures(mm)} (the output is not given here), you
will see that the data points 1 and 20 are marked influential.
Influence measures presents a set of measures each indicating whether
a particular data point is influential or not. For this exercise, we
will take highest `Cook's distance', whose critical values are  
represented by the dashed line contours plotted in \emph{residuals vs 
leverage} graph. The data point 1 has a larger Cook's distance. As a
result we will re-fit a model by removing this data point. Here is
once more how to do it, this time we only print the summary without
saving the resulting model as a variable:

\lstinputlisting{listings/ex-lr-influence2.txt}

Summary indicates that now the model is a better fit to the data. But,
you should probably check diagnostics as well. In general you should
only remove the data points if you have reasons other than making 
your model fit better.
\end{Answer}
\end{Exercise}

\subsection{Model selection}

In our example above, age seems to explain a lot, almost everything,
about the MLU (an effect of this importance is  due to  use of data 
from a single child). Also, even though we know from
Exercise~\ref{ex:lr-simple-reverse} that the mother's MLU was a
significant predictor of the child's MLU, with inclusion of other
variables it is not significant anymore. This is because of the fact
that almost all the variance \lstinline{mot.mlu} explains, is also
explained by other variables. When used together with others, TTR
seems to have a borderline significance.

Although there are no hard and fast rules, the best model choice
depends on a trade off between the best fit to the data (better
explanation), and models with fewer parameters (simpler model). 
The \lstinline{summary()} of the model helps us decide whether model fits
the data better. Here, we seek models with lower 
\lstinline{Residual standard error} 
and, consequently, higher $R^2$. The simplicity is more difficult to
characterize, but in the case of simple linear models, fewer
parameters indicate simpler models. You should note, though, you may
prefer a more complex model based on some other (scientific)
motivation.

One can either start with a `null model', and add variables until the
improvement in the model fit does not worth the increase in
complexity. Alternatively, we can start with the full model, and
remove variables until gain in simplicity by removing the least
explanatory variable is too damaging for the explanatory power.

\begin{Exercise}[label=ex:lr-model-simplification]
Starting from the full model with variables
\lstinline{mot.mlu}, \lstinline{chi.ttr} and \lstinline{age.m} for
explaining \lstinline{chi.mlu}, check the effects of removing each
explanatory variable from the model. For the best two-variable model, 
decide whether you'd remove one of the variables or not.
\begin{Answer}

We will do this the hard way first. We will create all relevant
models, and compare them according to the output of
\lstinline{summary()}. Instead of fitting the models from scratch, we
will do this using the function \lstinline{update()}. Remember that we
had the full model assigned to a variable \lstinline{mm}. The calls to
\lstinline{update()} below subtract each explanatory variable in turn.
This is our first use of dot `\lstinline{.}' in formula notation,
which roughly means `everything'. Since we do not change the response
variable, we skip the part before tilde. Here is the code:

\lstinputlisting{listings/ex-lr-model-simplification.txt}

The conclusions we can get from the above is that all models predict
the child's MLU well, but \lstinline{age} seems to be by far the best
predictor. The models that include age have a low residual error, and
high R-Squared, and age is a significant predictor in all cases. Since
the models are of the same complexity, everything else being equal
we'd prefer the model that explains most of the variation.  In this
case, \lstinline{m1} and \lstinline{m3} seems to be better, with
\lstinline{m1} explaining the variation a bit better (but the
difference is not very convincing).  
\end{Answer}
\end{Exercise}

A criterion for preferring a more complex model to a more simple model
is to check whether the explained variance is significantly higher for
the complex model. Remember that this is what ANOVA does.  The
function \lstinline{anova()} can be used to compare variances of two
model objects. For demonstration, the listing below compares the null
model with the full model.

\lstinputlisting{listings/lr-anova.txt}

The model formula \lstinline{var ~ 1} indicates a model that does not
depend on any other variable. In other words, a model whose only
parameter is the intercept, the mean of the dependent variable.  Not
surprisingly, the result is highly significant. 

The F-value reported the same F-value you also see in the summary of
the linear model. The default summary already tells us how our model
fairs compared to `the model of the mean', a horizontal line with
intercept equal to overall mean of the response variable.  You can
compare variances of any two models, not necessarily against the null
model using \lstinline{anova()}. However, you should note that F-test
used by \lstinline{anova()} does not care about the model complexity.

Another method that is used more commonly for model comparison is
called \emph{Akaike information criterion} (AIC). The AIC takes both
the model fit and model complexity (number of parameters) into
account.  The lower the AIC value, the better the model. The function
\lstinline{AIC} in \R{} computes the AIC for a model. 

\begin{Exercise}
Calculate the AIC for models 
\lstinline{chi.mlu ~ mot.mlu + age.m + chi.ttr} and 
\lstinline{chi.mlu ~ 1}. Note that you can supply two models to the
same \lstinline{AIC()} function call. Which model is preferable 
according to AIC?
\begin{Answer}
Assuming you have \lstinline{m.full} and \lstinline{m.null} as
described above, you need just the following line:
\lstinputlisting{listings/ex-lr-aic.txt}
Not surprisingly, \lstinline{AIC()} also indicates that the full 
model is better. You should note that this is different than 
the \lstinline{anova()} result. ANOVA only indicates that the full
models fits the data better than the null model. On the other hand, 
AIC takes the model complexity into account as well. In principle, the 
model that ANOVA favors may get a higher AIC (remember: lower AIC is 
better) value if it is more complex in terms of the number of 
explanatory variables.
\end{Answer}
\end{Exercise}

For a systematic model simplification (or model building) process, we
would add and remove the variables one by one, in a stepwise fashion.
With a few more variables, the manual search we did in
Exercise~\ref{ex:lr-model-simplification} gets too cumbersome to work
with. For this reason, \R{} also provides an automatic model refinement
procedure. The function \lstinline{step()} takes a model as an
argument and simplifies it based on the AIC.

\begin{Exercise}
Use \lstinline{step()} to find the minimal adequate model for 
\lstinline{chi.mlu ~ mot.mlu + age.m + chi.ttr}.
\begin{Answer}
Again, the command is simple (but you are encouraged to read its
documentation), we will include some description of the output though.

\lstinputlisting{listings/ex-lr-step.txt}

Given a model, \lstinline{step()} follows a step-wise removal
procedure. It starts with the full model with \lstinline{AIC=-80.08},
removing \lstinline{mot.mlu} provides the best (actually the only)
improvement. So in the next step, it considers the model with two
explanatory variables, \lstinline{chi.ttr} and \lstinline{age.m},
removing neither brings and improvement. As a result, it choses the
model \lstinline{chi.mlu ~ age.m + chi.ttr} as the best model.

\end{Answer}
\end{Exercise}
