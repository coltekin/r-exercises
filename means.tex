\section{\label{sec:twomeans}Comparing two means}

In this section, we will use the same data set we used in
Section~\ref{sec:vector}. In case you skipped the this sections, 
you should start with typing
\begin{lstlisting}[language={}]
> wc <- c(3497, 3495, 3456, 3506, 3503, 3515, 3486, 3504, 3541, 3469)
\end{lstlisting}

\subsection{\label{ssec:ttestsingle}One-sample T-test}

The data we have used so far was a set of word counts from 10 student
essays. The students were asked to submit an essay between 3500 to
4000 words. For the sake of demonstration, we will try to test our
(alternative) hypothesis that students tend to meet the minimum
requirements. And, we pick the null hypothesis that the mean of the
word-count should be the mid-way between the indicated upper and lower
limits, 3750 (you are encouraged to criticize the design of this
analysis). We will use Student's t-test for this purpose.

As you have already discovered in Section~\ref{sec:start}, the \R{}
function to do that is called \lstinline+t.test()+. Try the following
command,

\lstinputlisting[language={}]{listings/one-sample-ttest.txt}

If you know what the t-test is about, the output is self descriptive.
\R{} informs us that we did a `one-sample' test, gives us the value of
the t-statistic and associated p-value. It tells us that the
alternative hypothesis is `true mean is not equal to 3750'. Further it
gives us the 95\% confidence interval (this is an indication of the
uncertainty about the estimated population mean). The p-value 
indicate that null hypothesis is highly unlikely. p-value is really low (it is
$8.421\times10^{-11} = 0.00000000008421$, a very significant value for
any standard).

If you read the output carefully, you should have realized a problem
with this test. By default, \lstinline{t.test()} performs a two-tailed
test. Although we would not care much with this p-value, we originally
stated our alternative hypothesis as a directional hypothesis. We expect
the sample mean to be smaller than the hypothetical mean, not just different. Since t-distribution is
symmetric, you can, divide the p-value we found above by two. However, you can
also instruct \lstinline{t.test()} to perform a one-tailed test. 

\begin{Exercise}
Use \lstinline{t.test()} for one-tailed alternative
hypothesis. That is, population mean is less than 3750. You can find
the necessary parameter using \lstinline{help()}.
\begin{Answer}

\lstinputlisting[language={}]{listings/ex-ttest-one-tailed.txt}
\end{Answer}
\end{Exercise}

\subsection{\label{ssec:ttestindependent}Independent-samples T-test}

The one-sample t-test is most suitable if we have a theoretical mean
(for example a well-established population mean) to compare against.
In most cases, we compare means of two samples. We again use the same
function, \lstinline+t.test()+, for this purpose. For this test, we
need another set of word counts. We ask another, but different,
group of students to write an essay `\emph{around 3750-words}' on
the same subject, and get the following number of words for 10 essays.
\begin{lstlisting}
3541, 3463, 3487, 3838, 3870, 3700, 3871, 3977, 3960, 3469
\end{lstlisting}

Now we can do a two-sample t-test.
\lstinputlisting[language={}]{listings/two-sample-ttest.txt}

The output is slightly different than the one-sample case. Except that
we remembered to do a one-tailed test this time
(\lstinline{alternative='less'}), \lstinline{t.tests()} announces
itself as `Welch Two Sample t-test', and reported degrees of freedom
is a somewhat interesting value. The reason for this
change is that, assumption of equal variances is an important concern
for the validity of a t-test. 

By default \lstinline{t.test()} does not assume that variances are the
same and applies the necessary correction to the degrees of freedom.
You can change this assumption by \lstinline{var.equal=TRUE}, in which
case the resulting test will have higher power. However, how do we
know if we are confident that variances are the same? Here is a way to
check it:

\lstinputlisting[language={}]{listings/var-test.txt}

It seems the \lstinline{t.test()} was rightfully cautious.
Very convincingly, the variances are not likely to be equal.

\begin{Exercise}
Try \lstinline{t.test()} with \lstinline{var.equal=TRUE} option and 
compare the result with the test results you obtained earlier without 
this option. What type of error this wrong assumption may result in?
\begin{Answer}

\lstinputlisting[language={}]{listings/ex-ttest-eqvar.txt}
We obtain even a more significant result that the means are not the
same. In this particular case, the p-values are both rather low.
However, if we had a more borderline case, we may have rejected the
null hypothesis incorrectly, making a type-I error.
\end{Answer}
\end{Exercise}

Another, probably more crucial, assumption of t-test is that the
samples are taken from an approximately normal distribution. In this
section we take this for granted, we will return to this subject in
Section~\ref{sec:sstats} and \ref{sec:means-nonp}.


\subsection{Paired t-test}

If our data were paired, we want to do a paired t-test. Not
surprisingly, we again use \lstinline+t.test()+ for this task. The
data sets we worked are not paired, but for the sake of exercise we
will assume so. So, let's assume that second data set,
\lstinline{wc2}, contains the number of words in another round of
essays from the same students. We will further assume that we took all
necessary measures during the experiment design such that, except some
random factors, there is no important effects on word count other than
the instruction we gave (`between 3500--4000' or `around 3750' words).

Here is how to do the t-test after this unrealistic set of assumptions:

\lstinputlisting[language={}]{listings/paired-ttest.txt}

There is nothing surprising in the output. A paired-test generally has more
power than its independent-samples counterpart, and it is also true
for these not-particularly-paired sets of data.

\begin{Exercise}
Repeat the paired t-test in the example above, but sort both data
vectors before doing the test. Can you explain the difference in 
the p-value?
\begin{Answer}

\lstinputlisting[language={}]{listings/ex-sorted-ttest.txt}
We obtained a more significant result. The difference has to do with
the fact that the ordered lists make the differences between the means
more consistent. This is also what we would expect in a real paired
test. E.g., everything else being equal, students who tend to be 
more verbose would have higher word counts in both cases.
\end{Answer}
\end{Exercise}

%%%% MORE here.
