\section{\label{sec:probdist}Probability Distributions}

This section walks you through a set of utilities \R{} provides for
working with probability distributions. Probability distributions
underlie all statistical analyses we perform. In most cases you will
not use these utilities directly, \R{} provides functions to do the
analysis you are interested in without requiring to work with the
distribution functions. Nevertheless, it is important to understand
the concepts behind the analyses you are performing. If you read this
section and do the exercises here, you will refresh your memory about
probability distributions, and get a few additional tips and tricks about
using \R{}.

The probability distributions we have already mentioned directly or
indirectly include \emph{normal} (or \emph{Gaussian}) distribution,
\emph{Student's t} distribution, \emph{log-normal} distribution. There
are many other theoretical distribution that are interesting for
statistical analysis. Most
data in real life comes in the form of one distribution or another,
and in statistics we often assume that the data comes from a certain
distribution, typically the normal distribution. Even in
non-parametric tests, where we do not assume that the data is distributed
according to a theoretical distribution, we use the fact that a
relevant statistic is distributed (roughly) according to a well-known
probability distribution.

For each distribution it knows about, \R{} provides four functions that
may come handy at times. We will explain these functions using the
normal distribution.

\begin{description}
\item[\textbf{d}norm()] The functions that start with \lstinline+d+
are the probability \emph{density} functions for the continuous
distributions and probability \emph{mass} function for the discrete
distributions. Hence, \lstinline+dnorm()+ is the function that
produces the well known \emph{Bell Curve}. We have already made use of
\lstinline+dnorm()+ in Section~\ref{ssec:normtest}.
Figure~\ref{fig:dpnorm}a presents a graph of the standard normal
distribution function. 

A probability distribution is typically specified using a small number
of \emph{parameter}s. Most distributions can be parametrized many
different ways, but generally we have a canonical parametrization of
the distributions we work with. For example normal distribution can
parametrized by its \emph{mean} and \emph{standard deviation}. If you
do not supply the parameters, For all functions we study in this
section, the relevant parameters can be provided to give a particular
parametrization of the distribution. If a distribution has
\emph{standard} parameter values, \R{} will use the standard values if
you do not specify the parameters. For normal distribution this is
\lstinline+mean=0+ and \lstinline+sd=1+. 

\item[\textbf{p}norm()]  The functions that start with \lstinline+p+ are the
\emph{cumulative distribution} functions (CDF). Assuming your data follows a
theoretical distribution, cumulative distribution
functions gives the probability of observing a value lower than the
specified value. For example, \lstinline+pnorm(-2.5)+ will give you
the total probability of observing a value less than or equal to
\lstinline+-2.5+, given your data is distributed according to standard
normal distribution. Figure~\ref{fig:dpnorm}b presents the graph of
the CDF of the standard normal
distribution.

The CDFs are simply the way of obtaining the infamous \emph{p-values}. You
can use these functions instead of the p-value tables that decorate many
statistics textbook inner covers or appendixes. As a bonus, you can
provide parameters to these functions directly, avoiding conversion
between the standard scores to the scores you are interested in.

\item[qnorm()] The functions that start with \lstinline+q+ are the
\emph{quantile} functions. The quantile function of a distribution is
the inverse of its CDF. In other words, given a p-value, quantile
function returns the value \lstinline+x+ where probability of
observing \lstinline+x+ or less than \lstinline+x+ is \lstinline+p+.
If you are lost with the explanation don't worry (yet), the following example
may help. Assume that we want a p-value of .025, and we know that the
data is distributed according to standard normal distribution. If you
run the command  \lstinline{qnorm(0.025)} in \R{}, you will get a
familiar number. The number \R{} presents is the value of the variable
for which you would obtain a p-value of 0.05 (for a two-tailed test).
Similarly \lstinline+qnorm(0.005)+ will give you another familiar
number. Note that these p-values are half of the p-values
($\alpha$-levels) typically required for 2-tailed tests.

\item[rnorm()] The functions that start with \lstinline+r+ are
sampling functions. They return a vector of specified size that
is sampled randomly from a given distribution. For example,
\lstinline+rnorm(10)+ will give 10 random numbers that are distributed
according to standard normal distribution.
\end{description}

\begin{figure}
\begin{center}
\tikzinput{plots/dist-density-graphs}
\end{center}
\caption{\label{fig:dpnorm}Example graphs: (a) 
density function (b) cumulative distribution function of the standard
normal distribution.}
\end{figure}


\subsection*{Exercises}

\begin{Exercise}
For the normal distribution with $\mu = 3500$ and $\sigma = 200$, 
    \begin{itemize}
    \item find the probability that a value from this distribution is less than or equal to 3000.
    \item find the probability that a value from this distribution is greater than or equal to 4000.
    \item what is the probability that a value from this
    distribution is  between 3000 and 4000.
    \item find the probability density value for 3400. Is this value a probability?
    \item find the maximum value for which p-value is less than $0.01$.
    \item find the upper and lower bounds for which $p < 0.005$.
    \end{itemize}
\begin{Answer}

\lstinputlisting[language={}]{listings/ex-pdist-norm.txt}
\end{Answer}
\end{Exercise}


\begin{Exercise}
Plot probability density functions for the standard normal 
distribution ($\mu=0$, $\sigma=1$) and t-distribution with degrees 
of freedom 3, 10 and 30 on the same graph. Make sure all functions 
are plotted in the range -5 to 5 as smooth curves (not points), and 
choose different colors for each function.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-pdist-dplot.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/pdist-dplot}
\end{minipage}
\end{Answer}
\end{Exercise}


\begin{Exercise}[label=ex:pdist-binom]
\emph{Binomial} distribution characterizes \emph{n} trials of an
event with one of two outcomes. One of the outcomes occurs with probability
\emph{p}. For example, the binomial distribution with \emph{n=10} and
\emph{p=0.5} characterizes number of heads (or tails) you get for 10
flips of a fair coin. Every 10 flips you perform will produce
a number between 0 and 10 (more likely 5 than 1 or 9 though).

    \begin{itemize}
    \item Produce a random sample of 200 values from a binomial 
        distribution with $n=100$ and $p=0.55$, and plot its
        histogram. Plot a `normalized histogram' such that the
        area under the histogram sums to 1, and modify the axis to
        contain all possible values (not only the values in the
        sample).
    \item Plot the theoretical density (or more correctly mass) function 
    over the histogram.
    \end{itemize}
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-pdist-binom.txt}

A few points to note here: 
\begin{itemize}
\item Your histogram will be similar, but almost certainly different. 
Like other `\textbf{r}' functions, \lstinline{rbinom()} produces
random numbers (even though they are distributed according to
binomial distribution). 
\item The parameter \lstinline{probability=T} tells \lstinline{hist()} 
to plot a 'density' histogram instead of counts (or frequencies). This 
is equivalent to \lstinline{freq=F} we used before.

\item \lstinline{xlim=c(0,100)} 
\lstinline{xlim=c(0,100)} tells that the X-axis should be in range 0 to
100 (all possible values for a binomial distribution with n=100).
Likewise, \lstinline{ylim=c(0,01)} sets the range of the Y-axis to
make sure that the distribution function we plot next fits into the
graph regardless off the differences in the sampling (although, it is
not the best way).

\item Last, the \lstinline{curve()} function is an alternative way of
plotting smooth curves. Previously we used an alternative method for
drawing curves. If you followed this method, you probably used
something similar to:
\begin{lstlisting}
> lines(0:100, dbinom(0:100, size=100, p=0.55))
\end{lstlisting}
\end{itemize}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/pdist-binom}
\end{minipage}
\end{Answer}
\end{Exercise}


\begin{Exercise}
Repeat the previous exercise, but produce 20 and 2000 random samples
instead of 200. Observe the difference of the fit between 
the histogram and the theoretical distribution function.

\begin{Answer}

The code for this exercise is the same as
Exercise~\ref{ex:pdist-binom}, except you need to modify the first
parameter of the \lstinline{rbinom()} function (don't be shy to use
the command line history).

For ease of comparison, you may want to see two graphs side by side. 
Type the following prior to drawing your graphs to do that.
\begin{lstlisting}
par(mfrow=c(1,2))
\end{lstlisting}
This is an early tip about the possibilities of graphs in \R{}, we will
introduce more in Section~\ref{sec:graph}.

If all goes right you should see a better fit to the theoretical
distribution as you increase the size of the sample.
\end{Answer}
\end{Exercise}

\begin{Exercise}
We have used the \R{} function \lstinline+t.test()+ earlier in
Section~\ref{ssec:ttestsingle} to test whether mean of a given sample
is significantly different than a theoretical mean value.
\lstinline+t.test()+ is the easy way to do one- or two-sample t-tests.
In this exercise, we will do it the hard way.

First a bit of reminder from your introduction to statistics course:
one-sample \emph{t-statistic} (of the mean) can be calculated with the
following formula:
\[
    t = \frac{\bar{x} - \mu}{s / \sqrt{n}}
\]
where $\bar{x}$ is the mean of the sample, $\mu$ is the mean of the
theoretical distribution, $s$ is the sample standard deviation, and
$n$ is the sample size.

    \begin{itemize}
    \item Calculate the t-score for testing whether the mean of the
    population estimated from the sample \lstinline{wc} is different from the 
    theoretical mean value of 3750. Is the score you obtained the 
    same as the result you got with \lstinline+t.test()+.
    \item For the \emph{t} score calculated above, calculate the 
    p-value using \lstinline+pt()+. Note that we want to do a 
    2-tailed test (whether the means are different or not, 
    regardless of the direction of the difference).
    \item Which t-scores would give us a one-tailed p-value of $0.01$.
    \item Assuming that we have 10 data points, and standard deviation
    is 20, find the maximum value that gives you a p-value of 0.01 for
    the following hypothesis:
        \begin{itemize}
        \item The mean of the population is less than 3750.
        \end{itemize}
    \end{itemize}
\begin{Answer}

\lstinputlisting[numbers=left,language={}]{listings/ex-pdist-ttest.txt}
Line~1 is translation of the formula for the t-statistic into \R{}. Note that
this is the value we found in Section~\ref{ssec:ttestsingle} using
\lstinline{t.test()}. Line~3 calculates the p-value. Since we want to
do a two-tailed test, we multiply the value by we obtain with
\lstinline{pt()} by 2. Again, the p-value is the same with
\lstinline{t.test()}. Line~5 calculates t score on both tails after
which p-value drops under 0.01. To get the calculation in Line~8 
from the t-score formula given above you just need some simple
high-school math.
\end{Answer}
\end{Exercise}
