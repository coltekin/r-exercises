\section{\label{sec:bgraph}Basic graphs in \R{}}

Graphs are important tools for making sense out of our data and
communicating our results.  \R{} provides many graphical routines to
produce graphs that visualize data in useful ways (\R{} graphs can be
really pretty too!). We will work with few basic plotting utilities in
\R{} in this section, exercise with them in the next sections  and return
to more advanced use of graphs in Section~\ref{sec:graph}. If you
would like to see a sample of graphs produced in \R{} (including the code
to produce them!) the \emph{\R{} graph gallery} at
\url{http://addictedtor.free.fr/graphiques/} is a good place to have a
look.

Throughout this section, we will assume that you have the following
vector variables created in Section~\ref{sec:vector} and
\ref{sec:twomeans}. If you do not
have these variables, it is a good idea to create them. You can
cut\&paste the following for creating them.

\begin{lstlisting}
> wc = c(3497, 3495, 3456, 3506, 3503, 3515, 3486, 3504, 3541, 3469)
> zwc = c(-0.06759625, -0.15209156, -1.84199776, 0.35488030, 0.18588968, 
          0.56611858, -0.27883452, 0.31263265, 1.96029119, -1.03929231)
> wc2 = c(3541, 3463, 3487, 3838, 3870, 3700, 3871, 3977, 3960, 3469)
\end{lstlisting}

The basic two-dimensional plotting utility in \R{} is the
\lstinline+plot()+ function. In its typical use, \lstinline+plot()+ takes two
vector arguments of the same length, and plots the corresponding points
in the X-Y plane. If only one vector argument is given, then \R{} plots the
values in the vector against the index number of each value. Type the
following command (remember that you do not type the command prompt
\lstinline+>+)
\begin{lstlisting}
> plot(wc)
\end{lstlisting}
If everything went well, \R{} will spawn a new window where you should
see a graph of each data point (on the Y axis) plotted against their
index value (on the X axis). Your graph should look like 
Figure~\ref{fig:basicplots}a.

Although it demonstrates the use of the basic \lstinline+plot()+
command, this graph is not very useful. The use of \lstinline+plot()+
as a data exploration tool becomes more important when we have two
variables. If we plot two vectors against each other, we get a plot,
called \emph{scatter plot}. 

\begin{Exercise}
Plot a scatter plot of \lstinline{wc} (on the x-axis) against
\lstinline{wc2}(on the y-axis).
\end{Exercise}


%For the sake of demonstration,
%we will assume the essays our got the following grades (out of 100),
%presented in the order of words presented above.
%\begin{lstlisting}
%73, 83, 74, 69, 59, 51, 58, 83, 61, 57, 91, 93, 74, 75, 80
%\end{lstlisting}
%We can put the grades into another vector easily by,
%\begin{lstlisting}
%> grades = c(73, 83, 74, 69, 59, 51, 58, 83, 61, 57, 91, 93, 74, 75, 80)
%\end{lstlisting}
%and produce a scatter plot of k

Assume that we are curious about the difference of natural logarithm, and
the base-\lstinline+2+ logarithm functions. It would be nice to see
both plot of both functions together. The following example plots both
on the same graph. Note that you should not close plot window after the
first plot.

\begin{lstlisting}[numbers=left]
> x = 1:100
> plot(x, log(x, base=2), type='l', col='blue')
> lines(x, log(x), col='red')
\end{lstlisting}

This example introduces some handy notation that you will frequently need
while using \R{}. The first line creates a vector, \lstinline+x+,
containing the integers between 1 and 100. A more flexible way of
generating sequences is the \lstinline+seq()+ function (It is a good idea
to read the help text for \lstinline+seq()+. We will use it in the
exercises at the end of this section). The second line
plots the base-2 logarithm of the values in the vector.
The argument \lstinline+type='l'+ (\emph{note:} this is lowercase
\lstinline{L}, for `lines', not the number \lstinline{1}) tells \lstinline+plot()+ to
draw lines between the data points, resulting in a smooth looking curve if
data points are frequent enough. \lstinline+col='blue'+, on the other hand,
tells \lstinline+plot()+ to plot the lines in blue. In the third line we
used \lstinline+lines()+ instead of \lstinline+plot()+. Like the 
\lstinline+type='l'+ option of \lstinline+plot()+ \lstinline+lines()+ 
produces lines. However, more importantly, \lstinline+lines()+ do not
start a new graph. It draws over an existing graph. We could use
\lstinline+points()+ instead of \lstinline+lines()+ if we wanted data
points to be plotted on an existing graph instead of the lines between
each data point. Another interesting function of the same sort is
\lstinline+text()+ which prints arbitrary text on given coordinates.

\begin{figure}
\begin{center}
\tikzinput{plots/basic-plots}
\end{center}
\caption{\label{fig:basicplots}Example graphs: (a) plot of the vector
\lstinline+wc+, word counts of 10 student essays,  (b) comparison
of natural logarithm and the base-2 logarithm functions.}
\end{figure}

There is a lot more about graphs in \R{}. For now, we will stop here,
hoping that you are familiar with the basics. Graph related
subjects will be revisited when necessary, and a more comprehensive
(but still very limited) introduction will be provided in
Section~\ref{sec:graph}.

\subsection*{Exercises}

\begin{Exercise}
Plot a scatter plot of the vector \lstinline{wc} against
\lstinline{2*wc}. Use \lstinline+wc+ as your `x variable' and
\lstinline{2*wc} as your `y variable'.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-bgraph-scatter.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/bgraph-scatter}
\end{minipage}
\end{Answer}
\end{Exercise}


\begin{Exercise}
\begin{itemize} 
\item against \lstinline{2*zwc}.
\item against \lstinline+zwc^2+ on the same graph.
Use a different symbol, and a different color for the points 
(\textbf{TIP:} you can specify a different symbol with the option 
\lstinline{pch}). 
\end{itemize}
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-bgraph-scatter2.txt}
If you give a numeric value to \lstinline{pch}, instead of a
letter, you can get different shapes like rectangles, triangles,
diamonds and more.
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/bgraph-scatter2}
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}
Plot line segments passing through the following X-Y coordinates:
(0,0), (1,1), (2,3) and (4,4).
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\begin{lstlisting}
> x <- c(0,1,2,4)
> y <- c(0,1,3,4)
> plot(x, y, type='l')
\end{lstlisting}
Or on a single line:
\lstinputlisting[language={}]{listings/ex-bgraph-lineseg.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/bgraph-lineseg}
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}
In \R{}, you can draw a \emph{pie chart} with the function
\lstinline+pie()+. Plot a pie chart for the data used in
Exercise~\ref{ex:vec-sort}.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-bgraph-pie.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/bgraph-pie}
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}[label=ex:bgraph-sincos]
Draw \emph{sine}, \lstinline+sin()+, and \emph{cosine},
\lstinline+cos()+, functions in the range [-$\pi$, $\pi$]. Use
    a different color for each curve.\\
\textbf{TIP}: for smoother curves, you need to use 
    \lstinline+seq()+ to obtain data points with an interval smaller 
    than one, for example \lstinline+0.1+.\\
\textbf{TIP2}: \R{} defines a standard variable \lstinline+pi+ 
    with the value of $\pi$.
\begin{Answer}
\lstinputlisting[language={}]{listings/ex-bgraph-cossin.txt}
See Answer~\refAnswer{ex:bgraph-abline} for the result.
\end{Answer}
\end{Exercise}

\begin{Exercise}
The function \lstinline{abline()} draws a straight line whose
\emph{intercept} is \lstinline{a} and \emph{slope} is \lstinline{b}.
Using \lstinline{abline()}, add horizontal and vertical lines 
that pass from the origin (0,0) to the graph you produced in
Exercise~\ref{ex:bgraph-sincos}.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
First, a not-so-correct attempt:
\begin{lstlisting}
> abline(a=0,b=0)
> abline(a=0,b=10^50)
\end{lstlisting}
The horizontal line drawn first is straightforward, intercept and
slope are both 0. However, the way we drew the vertical line is a bit of
cheating. The slope of a vertical line is undefined (well, if infinity
is easier to deal with for you, we can also agree on positive infinity). As a 
result, we just put a very big number with the hope that approximation 
is fine enough. However, for horizontal and vertical lines, 
\lstinline+abline()+ provides a simpler and neater 
alternative,
\lstinputlisting[language={}]{listings/ex-bgraph-abline.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/bgraph-abline}
\end{minipage}

\end{Answer}
\end{Exercise}
