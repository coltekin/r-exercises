\documentclass{article}
\usepackage[hmargin=15mm,vmargin=15mm]{geometry}
\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{r-exercises}
\usepackage[answerdelayed,lastexercise]{exercise}
\usepackage[ddmmyyyy]{datetime}

\newcommand{\R}{\ensuremath{\mathsf{R}}}

%\usepackage{exercise}
%\newcounter{Example}
%\newenvironment{Example}%
%    {\begin{Exercise}
%    {\end{Exercise}}
\renewcommand{\ExerciseListName}{Exercise}

\renewcommand{\dateseparator}{-}

\title{R-Tutorial: A Case-Study on Mixed Design ANOVA}
\author{Lotte Schoot \& Çağrı Çöltekin}
\date{\small{\today,~\currenttime}}

\lstset{basicstyle=\tt\color{blue}}

\begin{document}
\maketitle{}

In this tutorial, we will investigate whether we can predict dyslexia before
children start to read. For this purpose, we learn how to use \R{} in performing a
Mixed Design ANOVA. The first part of this practical will be a brief
introduction to the topic. After introducing the problem, we can start
analyzing the data. We will first explore the data and then convert the data
into a format that can be used to perform a Mixed Design analysis. Next, we
will check whether necessary assumptions are met and finally, we will actually
perform a Mixed Design ANOVA-analysis and interpret the results. 

\subsection*{Background Summary}

In this study, we measured EEG-responses (electrical activity in the brain) of
17-months old children with an enlarged risk for dyslexia, while they were
listening to the sequence
\texttt{/ba-ba-ba-ba-ba-ba-ba-\textcolor{blue}{da}-ba-ba\ldots/}.\ Previous studies with
healthy adults have shown that while they were listening to this type of
sequence, the unexpected, deviant stimulus \texttt{/da/} elicited a stronger
reaction in the EEG signal than the standard stimulus \texttt{/ba/}. However,
the Phonological Deficit Hypothesis states dyslectics have a diminished ability
to discriminate between phonemes, and will therefore not notice the deviant
\texttt{/da/} in the sequence. The prediction that follows from this hypothesis
is that dyslectics will not show a significant difference between their
EEG-reactions to the standard \texttt{/ba/} and deviant \texttt{/da/}, whereas
non-dyslectics will show this difference. 

Studies in which children and adults with dyslexia are tested do indeed seem to
confirm this prediction. However, this does not tell us anything about the
direction of causality. Is the reduced reaction to the deviant (as compared to
the control group) the cause of dyslexia or does it follow from being dyslectic
that the reaction is reduced?  Therefore we tested 17-months old children with
a familiar risk for dyslexia, who have not yet learned how to read and write,
to see whether dyslexia can be predicted. As we do not know whether children
are dyslectic at this age, the group of children was followed until they were
8, when it becomes clear whether a child is dyslectic or not. 

The data we use here comes from 34 children for who it is known whether or not
they are dyslectic (17 are dyslectic, 17 are not). For all these children, the
average EEG-responses to the standard \texttt{/ba/} and deviant \texttt{/da/} are calculated
(measured when they were 17 months old). We will now analyze this data,
expecting an interaction between stimulus (standard / deviant) and diagnosis
(dyslectic / not dyslectic) if dyslexia can be predicted at such an early age.
This interaction is shown in figure 1. The reaction to the deviant for the
non-dyslectic group could be either positive or negative (as is shown in the
figure) What is important, however, is whether or not there is a difference
between standard and deviant for the dyslectic and the control group, not what
the polarity of the difference is. 


\subsection*{Mixed Design ANOVA}

We already know how to perform a one-way ANOVA (tutorial 11). This
analysis should be performed when there are more than two groups that
need to be compared on one dependent variable. In the one-way ANOVA,
there is only one factor of interest; the between-subjects factor.
However, in the current situation we have to deal with more than one
factor. Next to the between-subjects factor (diagnosis), there is also
a within-subjects factor (stimulus) because there is more than one
measurement for each subject (one average for the standard stimulus
\texttt{/ba/} and one for the deviant \texttt{/da/}).  Therefore, we
have to make use of a Mixed Design ANOVA, which is a combination of a
factorial ANOVA and a Repeated Measures ANOVA. In \R{}, we make use of
the Repeated Measures ANOVA technique in which we include a
between-subjects factor as well. Thus, the technique you are thought
here is also applicable for Repeated Measures without a
between-subjects factor. 

\subsubsection*{Before performing Mixed Design ANOVA}

First, we need to import the SPSS-like data-frame, using read.csv. An
extra argument  should be included in this command, which we have not
seen before. Dyslexia.csv is exported from a Dutch Excel file. As in
many other European countries, the convention is that the numeric
format uses a comma (e.g., \texttt{1,2223}), instead of a dot like
(e.g., \texttt{1.2223}).  \R{} does not like these commas instead of
dots, and will not see the dependent variables as numeric in our data
set if we do not specify this. Therefore, the option
\lstinline{dec=','} should be included in the \lstinline{read.csv()}
command. Additionally, the separator in this file is a semicolon
`\texttt{;}', which you should specify with the option
\lstinline{sep=';'}.

\begin{Exercise}
Load the data in the CSV file \texttt{dyslexia.csv} to a data frame 
called \lstinline{dys.wide}.
\begin{Answer}
\begin{lstlisting}
dys.wide = read.csv('C:\\Users\\Lotte\\Desktop\\Dyslexia.CSV', 
                    head=TRUE, 
                    sep=';', 
                    dec = ',')
head(dys.wide)
  Participant Diagnosis   Standard   Deviant
1           1       DYS -0.4158667  3.105000
2           2       DYS  4.7194000  4.734333
3           3       DYS -0.5210333 -1.412600
4           4       DYS -0.4038667 -2.536600
5           5       DYS  1.2766333  2.778700
6           6       DYS -1.0658333 -3.463367
\end{lstlisting}
\end{Answer}
\end{Exercise}

\begin{Exercise}
It is useful to look at your data before analyzing it. For this
purpose, make two sets of box plots in the same window (using
\lstinline{mfrow}) that show the distribution of the data for the
standard and deviant, for the dyslectic and the non-dyslectic groups.
Give the plots an appropriate name.

Keeping in mind that the hypothesis we are testing here states that
non-dyslectics show a larger difference between standard and deviant than the
dyslectics, can you already make a prediction about the outcome of the
analysis?
\begin{Answer}
\begin{lstlisting}
par(mfrow=c(1,2))
with(dys.wide[dys.wide$Diagnosis == 'DYS',], 
  boxplot(Standard, Deviant, 
          names=c('standard', 'deviant'), 
          xlab='Stimulus', 
          ylab='average ERP', 
          main='Diagnosis: dyslexia')
)
with(dys.wide[dys.wide$Diagnosis == 'NODYS',], 
boxplot(Standard, Deviant, 
        names=c('standard', 'deviant'), 
        xlab='Stimulus', 
        ylab='average ERP', 
        main='Diagnosis: no dyslexia')
)
\end{lstlisting}
\end{Answer}
\end{Exercise}

\subsection*{Assumptions}

For any parametric test, there are several assumptions that need to be
checked for the results of the test to be valid. Two important
assumptions are that of normality and homogeneity. 

\subsubsection*{Normal distributions per subgroup}

An important assumption in every ANOVA is that the data-distribution
within each subgroup is normal. Here, we have four subgroups: for both
the dyslectic and the non-dyslectic group we have EEG-measurements for
the standard \texttt{/ba/} and the deviant \texttt{/da/}. 


\begin{Exercise}
Make a QQ-plot for each of the subgroups (again, four plots in one window).
This will give a first indication whether the data are normally distributed.
Give the plots an appropriate name. When you have checked these plots, make
sure you also formally test the normality by using Shapiro-Wilk tests. 
\begin{Answer}

\begin{lstlisting}
par(mfrow = c(2,2))
with(dys.wide[dys.wide$Diagnosis == 'DYS',], 
     qqnorm(Standard, main='Dyslexia, Standard'))
with(dys.wide[dys.wide$Diagnosis == 'DYS',], 
     qqline(Standard))
with(dys.wide[dys.wide$Diagnosis == 'DYS',], 
     qqnorm(Deviant, main ='Dyslexia, Deviant'))
with(dys.wide[dys.wide$Diagnosis == 'DYS',], 
     qqline(Deviant))
with(dys.wide[dys.wide$Diagnosis == 'NODYS',], 
     qqnorm(Standard, main='No-Dyslexia, Standard'))
with(dys.wide[dys.wide$Diagnosis == 'NODYS',], 
     qqline(Standard))
with(dys.wide[dys.wide$Diagnosis == 'NODYS',], 
     qqnorm(Deviant, main ='No-Dyslexia, Deviant'))
with(dys.wide[dys.wide$Diagnosis == 'NODYS',], 
     qqline(Deviant))
with(dys.wide[dys.wide$Diagnosis == 'NODYS',], 
     shapiro.test(Standard)
# W = 0.9147, p-value = 0.12
with(dys.wide[dys.wide$Diagnosis == 'NODYS',], 
     shapiro.test(Deviant)
# W = 0.8902, p-value = 0.04672
with(dys.wide[dys.wide$Diagnosis == 'DYS',], 
     shapiro.test(Standard)
# W = 0.9173, p-value = 0.1328
with(dys.wide[dys.wide$Diagnosis == 'DYS',], 
     shapiro.test(Deviant)
# W = 0.9173, p-value = 0.1328
\end{lstlisting}
\end{Answer}
\end{Exercise}

\subsubsection*{Homogeneity of variances for the between-subjects factor}

For the between-subjects factor diagnosis (dyslectic/not-dyslectic),
we have to make sure that the variances between the levels of this
factor are equal. This needs to be checked for both levels of the
within-subjects factor: the reaction to the standard and the reaction
to the deviant. 

\begin{Exercise}
Using Levene's test (\lstinline{leveneTest()}), check whether there is
homogeneity of variances between dyslectics and non-dyslectics for the
standard and the deviant.  Levene's test is in the `\lstinline{car}'
library. You can check whether this package in on your computer by
typing \lstinline{library(car)} If the package is not yet on your
computer, you should be able to download it with
\lstinline{install.packages('car')} After this, the library-command 
(\lstinline{library(car)}) should work

\begin{Answer}
\begin{lstlisting}
library(car)
leveneTest(Standard,Diagnosis)
Levene's Test for Homogeneity of Variance (center = median)
      Df F value Pr(>F)
group  1    0.28 0.6004
      32          

>leveneTest(Deviant,Diagnosis)

Levene's Test for Homogeneity of Variance (center = median)
      Df F value Pr(>F)
group  1  0.0043 0.9484
      32    
\end{lstlisting}

Levene's test tests the assumption that the variances are equal. If
the outcome of the test is significant, this assumption does not hold.
Here, both tests are not significant. So, both for Standard as for
Deviant responses, the variances of the groups (dyslectics and
non-dyslectics) do not differ significantly. 
\end{Answer}
\end{Exercise}


\subsubsection*{Data-file conversion}

For Repeated Measures ANOVA (and thus also Mixed Design ANOVA), the
data-file needs to be converted. In an SPPS-like dataset, as we have
here, there is one row for each subject (Table~\ref{tbl:data-format}a). 

\begin{table}
\centering
\begin{tabular}{|l|l|l|l|}\hline
Subject &Diagnosis      &Stimulus-Standard &Stimulus-Deviant\\\hline\hline
1       &Dyslexia       &0.8188             &0.7289\\\hline
2       &Non-Dyslexia   &0.6897             &0.9921\\\hline
\ldots  &\ldots         &\ldots             &\ldots\\\hline
\end{tabular}\\
(a)\\~\\
\begin{tabular}{|l|l|l|l|}\hline
Subject    &Diagnosis       &Stimulus   &ERP-amplitude\\\hline\hline
1          &Dyslexia        &Standard   &0.8188\\\hline
1          &Dyslexia        &Deviant    &0.7289\\\hline
2          &Non-dyslexia    &Standard   &0.6897\\\hline
\ldots  &\ldots         &\ldots             &\ldots\\\hline
\end{tabular}\\
(b)\\~\\
\caption{\label{tbl:data-format}(a) The original `wide' data format.
(b) The format required by \R{} (and some other software packages
such as SAS): one variable per column.}
\end{table}

However, \R{} does not like the fact that the variable \lstinline{stimulus} is
listed in two columns. To be able to perform a Repeated Measures ANOVA
in \R{}, we need to have one column for each variable, as is shown in
Table~\ref{tbl:data-format}b. Subjects now have two rows, as they are 
measured on two stimuli. 


We can change the dataset that we are working with here into this
format by using the following code in \R{}. (Here, we assume that the
data-frame you have loaded \texttt{dyslexia.csv} into is called
\lstinline{dys.wide})

The first step in converting the data-frame is to combine the values
of `\lstinline{Standard}' and `\lstinline{Deviant}' into one large vector.

\begin{lstlisting}
erp.all = c(Standard, Deviant)
\end{lstlisting}

The next step of the data-file conversion is to make a new dataframe
(called `dys' here). As we will have two rows for each participant,
the values in the vectors \lstinline{Participant} and
\lstinline{Diagnosis} is replicated in line~1 and 2 below. 
\lstinline{as.factor()} in line~1 is included because
the values that make up the variable \lstinline{subj} are numbers, 
but they are names for subjects and should not be seen as numeric data.
Furthermore, we need to create a new column that indicates whether the
response is for the deviant-stimulus or the standard-stimulus (line~4).


\begin{lstlisting}[numbers=left]
dys=data.frame(subj=as.factor(rep(dys.wide$Participant, 2)),
               Diagnosis=rep(dys.wide$Diagnosis, 2), 
               Stimulus=rep(c('Standard', 'Deviant'), each=34), 
               Erp=erp.all)
\end{lstlisting}

We now have a new dataframe (dys), which we can work with for the
\emph{repeated measures analysis}.

\begin{lstlisting}
> head(dys)
  subj Diagnosis Stimulus        Erp
1    1       DYS Standard -0.4158667
2    2       DYS Standard  4.7194000
3    3       DYS Standard -0.5210333
4    4       DYS Standard -0.4038667
5    5       DYS Standard  1.2766333
6    6       DYS Standard -1.0658333
\end{lstlisting}

We see that the data frame does not look exactly like the example in
the table above. This is because the responses are not ordered per
participant, but per stimulus-type. This is okay for the analysis,
though, so we do not bother ordering them. However, you should check
whether all responses are in the data frame. 


\begin{Exercise}
Convert the data-frame \lstinline{dys.wide} according to the steps described 
above. Make sure to check whether your converted file looks the same as in 
the example above and that all responses are in the data frame.. 
\end{Exercise}
\begin{Answer}
\begin{lstlisting}
attach(dys.wide)
erp.all = c(Standard, Deviant)
dys=data.frame(subj=as.factor(rep(dys.wide$Participant, 2)),
               Diagnosis=rep(dys.wide$Diagnosis, 2), 
               Stimulus=rep(c('Standard', 'Deviant'), each=34), 
               Erp=erp.all)
head(dys)
dys
\end{lstlisting}
\end{Answer}


\subsubsection*{Further Assumptions}

For Mixed Design ANOVA or Repeated Measures ANOVA, there are several
assumptions that should be checked before running the analysis. Next to
normality and homogeneity, these include: 

\textbf{No independent samples}

There are multiple measurements per subject, so a normal F-test cannot
be used here. It will lack accuracy because it is based on independent
samples. Therefore, we have to make use of a Repeated Measures ANOVA.
Sphericity for the within-subjects factor For within-subjects factors,
another assumption needs to be checked: that of sphericity. This means
that the variances of the differences between the levels of this
factor need to be equal. However, the within subjects factor stimulus
has only two levels: standard and deviant. The variances of the
differences will thus be the same, and, therefore, sphericity is
always assumed here.  However, if there are more than two levels for
the within-subjects variable, this assumption should be checked! The
results of Mauchly's test for Sphericity and possible corrections are
then reported in the output of the function \lstinline{ezANOVA()} described
below. 

\subsection*{Mixed Design ANOVA}

Now that all assumptions are checked, we are ready to perform the
actual ANOVA. To do that, we will make use of the package
\lstinline{ez}.  

\begin{Exercise}
Make sure that the library  \lstinline{ez} is installed on your
computer (as we did for the \lstinline{car} library).  The command
\lstinline{library('ez')} include the commands from the library in
your \R{} environment, and it will fail if the package is not installed
on your computer. If it is not, type
\lstinline{install.packages(`ez')}. And then, try the
\lstinline{library(ez)} command again. 
\end{Exercise}

Now, for the Mixed Design ANOVA, as with One-way ANOVA and Regression Analysis, we will first create a model, that we are going to inspect later on. You can create this model as is described here. 

\begin{lstlisting}
m = ezANOVA(data = dys, 
              dv = .(Erp), 
             wid = .(subj), 
         between = .(Diagnosis), 
          within = .(Stimulus))
\end{lstlisting}

To see the ANOVA-results, you should use the command \lstinline{print(m)}.

\begin{Exercise}
Do the Mixed Design ANOVA-analysis, inspect the model and plot an
interaction-plots to see whether the data confirm our hypothesis that
non-dyslectics differ in their reaction to the standard and deviant,
while dyslectics no not show this difference. 
\begin{Answer}
\begin{lstlisting}
m = ezANOVA(data = dys, dv = .(Erp), wid = .(subj), between = .(Diagnosis), within = .(Stimulus))
print(m)

              Effect DFn DFd         F            p p<.05        ges
2          Diagnosis   1  32  3.343992 0.0767850549       0.07927735
3           Stimulus   1  32 16.056233 0.0003433891     * 0.08116136
4 Diagnosis:Stimulus   1  32 15.864167 0.0003673767     * 0.08026843
\end{lstlisting}

There is a significant main effect of stimulus
(\lstinline{Standard}/\lstinline{Deviant}) and a significant
interaction between Diagnosis and Stimulus. This means that the effect
of Stimulus is not the same in for each group (dyslectics and
non-dyslectics). To further inspect this difference, we need to plot
the interaction between stimulus and diagnosis.
\lstinline{interaction.plot} takes three arguments: the first
represents the x-axis (\lstinline{stimulus}), the second the different
lines (\lstinline{Diagnosis}) and the third the dependent variable
(\lstinline{Erp}).

\begin{lstlisting}
interaction.plot(Stimulus, Diagnosis, Erp)
\end{lstlisting}

So, we see here that our hypothesis is confirmed: for dyslectics,
there is no difference between the reaction to the standard
\texttt{/ba/}  and the deviant \texttt{/da/}, indicating that they do
not notice the difference between the two phonemes. On the other hand,
the non-dyslectics do show a significant difference between standard
and deviant, because they are able to notice the difference.  As the
current  data come from 17-months old children,  we would be able to
say that the ability to discriminate between subtle phoneme
differences, as measured by EEG, is a predictor for dyslexia. It is
very useful to be able to predict dyslexia, as early intervention has
been shown to have a significant effect on the ability to read and
write for dyslectics. Unfortunately though, the data is heavily
adapted to confirm the hypothesis and in thus fictitious. In real
life, therefore, we cannot say (on the basis of the current data) that
the difference between standard an deviant reaction is a predictor for
dyslexia. 
\end{Answer}
\end{Exercise}

\section*{Answers}

\shipoutAnswer

\end{document}
