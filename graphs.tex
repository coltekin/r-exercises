\section{\label{sec:graph}More on graphs}

The basics of graphs in \R{} were presented in Section~\ref{sec:bgraph},
and we have worked with a few common graphs throughout this tutorial.
So far, we plotted graphs on the screen, and we did not worry about
the cosmetics much. This section will give you further hints about how
to prepare functional and nice-looking graphs.  For most of this
section, we are concerned with how graphs look, rather than their
content. 

We will continue using the data set we used in
Section~\ref{sec:linear}. You should already have the data 
that we will used in this section in your \R{} environment. However,
Exercise~\ref{ex:graph-data} below makes sure that you have the
correct data for the rest of this section. A brief explanation of what
the data is about can be found Section~\ref{sec:linear}.

\begin{Exercise}[label=ex:graph-data]
Load the data in the CSV file
\href{http://www.let.rug.nl/~coltekin/R/data/childes-example2.csv}{childes-example2.csv}
to a data frame called \lstinline{childes}.
\begin{Answer}
\lstinputlisting[numbers=left,language={}]{listings/ex-graph-data.txt}
\end{Answer}
\end{Exercise}


\subsection{Labels, axes, legends \ldots}

When you use graphics in your presentations and publications, it is
important that the labels on the graph is intelligible to your
audience. For example, if you go back to simple scatter plot you plotted in
Exercise~\ref{ex:lr-simple-reg}, you will see that \lstinline{plot()}
put the variable names, like \lstinline{childes$mot.mlu} and
\lstinline{childes$chi.mlu}, as axis labels. This is most of the time
explanatory enough for the researcher who is familiar with the
data. However, for wider audience, we should use better
axes labels. Besides labels for axes, you can also specify a title
which is typically plotted on top of the graph. All three labels can
be specified to \lstinline{plot()} and related functions as additional
parameters. The parameters \lstinline{xlab}, \lstinline{ylab} and
\lstinline{main} will set the x-axis label, y-axis label and the title
respectively.

\begin{Exercise}[label=ex:graph-labels]
Produce a scatter plot demonstrating the relation between the mother's and
child's MLU scores, taking the child's MLU as the independent variable. Assign 
explanatory labels for x and y axes, and set a title.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-graph-labels.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/graph-labels}
\end{minipage}
\end{Answer}
\end{Exercise}

The \lstinline{plot()} function plots a small circle for
each pair of x-y values by default. Earlier, we saw that we can use different
characters, or shapes using the parameter \lstinline{pch}. With
\lstinline{pch} we either specify a single character or number, or a
vector that is the same length as the number of data points. In the first
case, all points are plotted using the same shape (e.g., circle or
triangle) if a number id given, or a alphanumeric character if a
character is given. If you provide a vector, you can specify the shape
or character used for each individual point.

\begin{Exercise}
Plot the same scatter plot in Exercise~\ref{ex:graph-labels}, but use
different symbols and different colors for data points that correspond
to age 2 and age 3.
For example, you should plot data 
points that correspond to age 2 with a red circle, while plotting 
data points that correspond to age 3 with a blue rectangle.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-graph-pch.txt}
The only interesting addition here is the \lstinline{rainbow()}
function. It returns a vector of specified length with colors
that are distinct from each other. \lstinline{gray.colors()} can be
useful if you intend to use your graph in black and white media only. 
The documentation for \lstinline{rainbow()} also lists a few other 
alternatives that may be more suitable for both color and black-and white.

You can find which numbers correspond to which symbol in the documentation
of \lstinline{points()}.
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/graph-pch}
\end{minipage}
\end{Answer}
\end{Exercise}

We have already seen how to draw lines that connect given X-Y
coordinates (using the parameter \lstinline{type='l'}, or function
\lstinline{lines()}). A line graph does not make much sense if the
independent variable is not (naturally) ordered (if you wonder why,
you are encouraged to reproduce the above scatter plot with line
segments in between, and see). One of the best case for line plots are
where independent variable (x-axis) is time.  We have also seen that
if we use functions \lstinline{lines()}, \lstinline{points()} or
\lstinline{text()}, resulting graph will be overlaid on the existing
graph. The following exercise will refresh your memory about these
functions, while adding some new information.

\begin{Exercise}[label=ex:graph-yrange]
For the graphs below use lines, and for two graphs you are asked to
plot, use different colors and line types.
\begin{itemize}
\item Plot progression of child's MLU by age (i.e., use \lstinline{age.m} as the
x-axis, \lstinline{chi.mlu}). 
\item Plot the mother's MLU by child's age on the same graph. Do you see both graphs? 
If you do, you are ahead of the exercises. Otherwise, following next
part will tell you why you did not, and how to fix it.
\item For all plots, \R{} attempts to find the best range for x-axis and
y-axis automatically. However, this happens only for a new graph. Once axes are plotted
there is no return. When you need custom axis ranges, you can manually specify
them using \lstinline{xlim} and \lstinline{ylim} parameters of
\lstinline{plot()} or related functions. Both parameters take a vector with two
numeric members, the first one specifies the lower limit, and the second
one specifies the upper
limit. For example, \lstinline{xlim=c(0,10)} sets up the x-axis such that 
 minimum value is 0, and maximum value is 10, regardless of the data.

Repeat the previous steps to plot the progression of the mother and
child's MLU by child's age on the same graph. Make sure the full range
is visible on the graphs, and use different colors for both.
\textbf{TIP/Reminder:} you can use \lstinline{range()} function to obtain range of
a vector.
\end{itemize}
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-graph-yrange.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/graph-yrange}
\end{minipage}
\end{Answer}
\end{Exercise}

When you have more than one line, or different sets of data points
displayed on the same graph, it is customary, and useful, to plot a
\emph{legend}. A legend gives brief information regarding what colors,
line or point types on the graph correspond to. In \R{}, you can use
\lstinline{legend()} function to add a legend to an existing graph.
Legend simply takes a set of descriptive strings as a vector, and
parameters that specify color and other stylistic variables. The
parameter names match with the equivalent parameters of
\lstinline{plot()}. As always, you can get more information,
including a few examples in the \R{} documentation.

\begin{Exercise}
Add a legend to the graph you produced in Exercise~\ref{ex:graph-yrange}.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
This listing only shows the additional \lstinline{legend()} 
command, the rest of the graph is the same as in Exercise~\ref{ex:graph-yrange}.

\lstinputlisting[language={}]{listings/ex-graph-legend.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/graph-legend}
\end{minipage}
\end{Answer}
\end{Exercise}

The colors are useful for distinguishing different objects in a graph.
However, we often need graphs that are also legible in black and
white. One way to do this is using different line types, e.g., solid,
dashed, dotted, etc. The \lstinline{lty} option we used before allows
us to draw different line types. If the data points in the x-axis is
relatively few another method is to plot lines, but also plot points
with different shapes. You can do this with \lstinline{type='b'} (for
`both') parameter of \lstinline{plot()} or \lstinline{lines()}. As
before, you can pick the shape drawn for the points using the
parameter \lstinline{pch}.

\begin{Exercise}
Repeat the plot in Exercise~\ref{ex:graph-yrange} using both lines and
points, using different colors and using letters 'c' and 'm' for the
points drawn for MLU of the child and mother respectively. Do not
forget to add a legend that matches the new graph.
\begin{Answer}
\lstinputlisting[language={}]{listings/ex-graph-both.txt}

Additional parameters \lstinline{pch} and \lstinline{type='b'} in all
relevant commands are expected. The only real news in this listing is
the parameter \lstinline{bty='n'} (read, `box type none') of
\lstinline{legend()}.  This removes the box drawn around the legend.
Here is the resulting plot:

\tikzinput{plots/exercises/graph-both}
\end{Answer}
\end{Exercise}

\subsection{More than one graph on the same canvas}

In some cases it is a good idea to include multiple related graphs in
the same figure. As well as helping you fit more in a paper with page
limits, it may also enhance the exposition of the data. 

So far we have changed the appearance of the graphics with options
directly given to functions like \lstinline{plot()} or
\lstinline{lines()}. You can also set quite a few parameters that
affect how graphs are drawn using \lstinline{par()} command. Some
parameters can be set both by \lstinline{par()} and plotting commands,
some can be set only by one or the other. You can always refer to the
documentation if you want to know where to state a certain parameter.
The current parameters of interest allow us to set up a matrix of
graphs on the same `canvas'. The parameters \lstinline{mfrow} and
\lstinline{mfcol} take numeric vectors of size 2. For both parameters,
the first number specify the number of rows of graphs you want to fit
into the canvas, and the second one specifies the number of columns.
For examples the command \lstinline{par(mfrow=c(4,5)} four rows and
five columns where next 20 \lstinline{plot()} (or related, like
\lstinline{hist()}, \lstinline{boxplot()}) commands will place
their output. The difference between \lstinline{mfrow} and
\lstinline{mfcol} is the order successive plots.
\lstinline{mfrow} fills the grid row by row, while \lstinline{mfcol}
fills the columns first.  Figure~\ref{fig:mfrowcol} demonstrates the
order of graphs drawn on a 2 by 2 matrix.

\begin{figure}
\begin{center}
\tikzinput{plots/graphs-mfrowcol}
\end{center}
\caption{\label{fig:mfrowcol}Order of graphs in 2x2 plots set up by (a)
\lstinline{mfrow} and (b) \lstinline{mfcol}.}
\end{figure}

\begin{Exercise}
Plot three graphs, histogram, box plot and normal Q-Q plots for 
\lstinline{childes$chi.mlu} %stopzone 
on the same figure, side by side. Make sure that all graphs have
sensible titles and axes labels.
\begin{Answer}
\lstinputlisting[language={}]{listings/ex-graph-side-by-side.txt}
\tikzinput[half]{plots/exercises/graph-side-by-side}
\end{Answer}
\end{Exercise}

We will not go through all possible ways of plotting multiple
graphics on the same figure. Setting \lstinline{mfrow} or \lstinline{mfcol} should work
for most cases. If you need more complicated settings, you
can check the function \lstinline{layout()} and graphical parameter
\lstinline{fig}, as well as the \lstinline{lattice} library. Many
other libraries and packages, such as \lstinline{gplots}, provide many
useful functions for generating graphics that are used
often.

\subsection{Writing your graphs to external files}

Once you are happy with your graph, you will want to include it in
your presentations and/or in your publications. You can, of course,
get a screenshot, but in most cases, this method produces less than
optimal
graphics, especially for publishing. \R{} supports a number of
`\emph{devices}' for plotting your functions on. The possible file
formats include postscript, pdf, PNG and JPEG. Typically, if you want
to have a bitmap file (for web and possibly for your presentations),
you should use \emph{PNG} graphics. If it is for a publication, you should
pick a \emph{vector} file format, such as \emph{PDF} (If you are a
\LaTeX{} user, you should definitely look at \lstinline{tikzDevice},
though).

To plot your graphics to an external file, you first need to use
appropriate function to initialize the output device. These functions
are typically the (lowercase) name of the graphics format you are
interested in. For example, \lstinline{pdf()},
\lstinline{postscript()}, \lstinline{png()} or \lstinline{tiff()}.
Initialization differs somewhat depending on the file type you want to
produce, but you specify a filename and the width and height of
the resulting graphics. For bitmap graphics, the width and height  are
specified in pixels, for vector graphics it is specified using a
measurement like inch. You should consult the documentation of the
function you are interested. In general it is important to specify the
right size since some properties of the resulting graph, such as font
sizes and line thickness, will be determined based on the size of the
graphics. Once you have initialized the output, the commands you use for
producing graphs are the same.  When you are done with plotting your
graph(s), you should type \lstinline{dev.off()}.  The resulting
graphics will be written to the file you specified during the
initialization. 

\begin{Exercise}[label=ex:graph-pdf]
Plot two sets of box plots side by side, one comparing child's and mother's
MLU, the other comparing their type/token ratio. Make sure your titles
and axis labels are reasonable (read documentation of
\lstinline{boxplot()} to see how to set labels of individual boxplots. Write the output
to a PDF file that is suitable to be included in a A4-size publication
with half-inch margins on both sides. The width of an A4 paper is 8.27
inches, and you probably do not want to fill the whole paper, so you
should use an image height about half of the image width.
\begin{Answer}
\lstinputlisting[language={}]{listings/ex-graph-pdf.txt}

The output should be similar to this figure:

\tikzinput{plots/exercises/graph-pdf-output}
\end{Answer}
\end{Exercise}

\begin{Exercise}
Repeat Exercise~\ref{ex:graph-pdf} two times for producing PNG
graphics of different sizes, 1024x512 (width x height) and 640x320. 
Display and compare the quality of the resulting graphics. 
\begin{Answer}

Here is only the sequences of commands to produce the PNG with
resolution 1024x512. 

\lstinputlisting[language={}]{listings/ex-graph-png.txt}

If you use bitmap graphics a lot, you'll realize that they scale
poorly on higher resolution medium, particularly on paper. If you
create bigger images, and scale them when necessary, the text 
on the graphs will typically become poorly readable, or even 
unreadable. To balance the looks at right sort of medium, you 
need to also specify the resolution (\lstinline{res}). By default
bitmap graphics are screen optimized (resolution is 72dpi), you should
change this to match the printer when you need to use them on paper
(for printing, you should really use vector graphics, unless there 
is a good reason for using bitmap). 
\end{Answer}
\end{Exercise}

%lowess
%loess
%gam
%lm
%bubble plot
%sunflowerplot
%orientation
%subscripts/expressions

\subsection*{Exercises}

We have neither space nor time to go through all possibilities related
to graphics in \R{}, but the following exercises will bring up a few
other points which you may use for your work. You are encouraged to
skim through the documentation of \lstinline{par()} to see the
range of customizations possible using graphical parameters. Other
functions in the base package graphics (you can obtain by typing
\lstinline{help(package='graphics')}) can also give you ideas about
what type of graphs and customizations are possible in \R{}. 


\begin{Exercise}[label=ex:graph-ttr]
\begin{itemize}
\item Plot a scatter plot of the child's type/token ratio (TTR) against 
mother's TTR. Assign descriptive labels for axes and a main title.
\item Fit a simple regression model to the plot you produced, and plot the
regression line on the same plot.
\item Include a text stating the amount of variance explained. Your
text should be similar to $r^{2} = 0.8$, but you should use the real
value for $r^{2}$. \textbf{TIP:} you can use \lstinline{expression()}
to produce subscripts and superscripts (as well as some other
mathematical functions). For example, \lstinline{expression(r^2)} 
produces $r^{2}$ and \lstinline{expression(r[2])} produces $r_{2}$.
Documentation of the function \lstinline{text()} include some useful
examples.
\end{itemize}
\begin{Answer}



\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-graph-ttr.txt}
Note that you need to use double equation symbol `\lstinline{==}' in
expressions.

The text placement requires you to first see $r^2$ and the suitable
coordinate values on screen before placing the text. Here is more
intelligent but rather a cryptic alternative, that also shows that you
can not only set graphical parameters with \lstinline{par()}, but also
get the current values of them.

\begin{lstlisting}
text(par('usr')[1]+2*par('cxy')[1],
     par('usr')[4]-2*par('cxy')[2],
     pos=4,
     substitute(R^2 == r2,
     list(r2 = round(cor(mot.ttr, chi.ttr)^2, 2))))
\end{lstlisting}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/graph-ttr}
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}
Remember that, the data we are working with in this section includes utterances from
mother--child interactions. The conversation is necessarily
asymmetric: at the beginning we expect child to participate less,
but he/she should be catching up later. We would like to visualize the
progression following five variables to see whether the values
converge in time.
\begin{itemize}
\item Number of utterances in each recording session:
\lstinline{chi.utt} vs.\ \lstinline{mot.utt}.
\item Ratio of utterances in each recording session:
\lstinline{chi.utt/(chi.utt+mot.utt)} vs.\ \lstinline{mot.utt/(chi.utt+mot.utt)}.
\item Ratio of morpheme tokens in each recording session:
\lstinline{chi.mtok/(chi.mtok+mot.mtok)} vs.\ \lstinline{mot.mtok/(chi.mtok+mot.mtok)}.
\item Type to token ratios in each recording session:
\lstinline{chi.ttr} vs.\ \lstinline{mot.ttr}.
\item MLU values in each recording session: \lstinline{chi.mlu} vs.\ \lstinline{mot.mlu}.
\end{itemize}

Plot all four graphs on a single pdf file such that all graphs are
shown on two rows. Use different colors and line types
for the mother and child.
Use descriptive labels for axes and include a title on each graph.
Include a legend on the last remaining slot.
\begin{Answer}
This exercise requires a lot of repetitive typing, you can use command
line history to ease the pain a bit. The solution below makes use of a
programming trick to reduce some repetition and save some space. We
define a custom function \lstinline{simple_plot} that plots one of the
plots we are interested given the data, and call it repetitively. We
could also wrap this part into a \emph{loop}, but this much
programming is enough for the day.

\lstinputlisting[language={}]{listings/ex-graph-3x2.txt}

\tikzinput{plots/exercises/graph-3x2}
\end{Answer}
\end{Exercise}

\begin{Exercise}
You should have already got the impression that almost every aspect of
the \R{} graphics are customizable. In this exercise, we will experiment
with two aspects. First, sometimes you like to enlarge or reduce the
size of the text on graphs. Various parameters that are used for
adjusting text starts with string \lstinline{cex} (for `character
expansion'). 
%Second, we sometimes want to reduce (or enlarge) margins
%where labels are printed. This is controlled by the parameters
%\lstinline{mar} and \lstinline{oma}. 
For the exercises below, you can find the necessary parameter value 
you need to change in the documentation of \lstinline{par()}.
\begin{itemize}
\item A plot with \lstinline{type='n'} sets up a new plot, but does not
plot anything (note that you have to provide a dummy argument to
plot, but it will be ignored). Start such a plot with arbitrary values for axes
ranges, labels, and a title. Make sure that font size of the title is half of
the default title size, and font sizes of the axis labels are twice
their default size.
\item Place a text, for example \lstinline{"NE"}, in upper left
corner of your plot area.
\item Place a text, for example \lstinline{"SE"}, in lower right 
corner of your plot area. Make sure the character size is double of
the default character size.
\item Place a single dot using
\lstinline{points(..., pch=20)} in the middle of your plot area.
\item Place a single standard point in the middle, but make sure that
it's size is 2.5 times the normal size.
\end{itemize}
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-graph-cex.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/graph-cex}
\end{minipage}
\end{Answer}
\end{Exercise}

\begin{Exercise}
Re-produce the scatter plot of mother's TTR against child's TTR in
Exercise~\ref{ex:graph-ttr} such that the size of the data points are 
proportional to the number of utterances in the related recording
session. You should use the default size for the minimum number of 
utterances, and increase the size of the circle for higher values.
\begin{Answer}

\begin{minipage}{0.5\textwidth}
\lstinputlisting[language={}]{listings/ex-graph-bubble.txt}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\tikzinput[half]{plots/exercises/graph-bubble}
\end{minipage}

The plot above is called a `\emph{bubble}' plot which is typically
used for displaying the effect of a third variable in a scatter plot.
Another option is to use \lstinline{sunflowerplot()} which is 
particularly useful when the third variable of interest is discrete,
such as number of participants or subjects in a study. You can try
the following to see this alternative.

\begin{lstlisting}
> sunflowerplot(chi.ttr, mot.ttr, floor((mot.utt + chi.utt)/2000))
\end{lstlisting}

\end{Answer}
\end{Exercise}

\begin{Exercise}
Replicate the graph on Figure~\ref{fig:mfrowcol}.
\begin{Answer}
Although the actual code that generates Figure~\ref{fig:mfrowcol} is slightly different, the following produces a
very similar figure.
\lstinputlisting[language={}]{listings/ex-graph-order.txt}
\end{Answer}
\end{Exercise}

